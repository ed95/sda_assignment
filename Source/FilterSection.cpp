//
//  FilterSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#include "FilterSection.hpp"

FilterSection::FilterSection(Audio& audio_, PresetSection& presetSection_) : audio (audio_), presetSection(presetSection_), lowPassLabel("", "LP"), highPassLabel("", "HP"), bandPassLabel("", "BP"), envelopeLabel("", "ENV"), cutoffLabel("", "CUTOFF"), qLabel("", "Q")
{
    addAndMakeVisible(&lowPassButton);
    lowPassButton.addListener(this);
    lowPassLabel.setFont(Font(11.f));
    addAndMakeVisible(&lowPassLabel);
    lowPassButton.setRadioGroupId(1);
    
    addAndMakeVisible(&highPassButton);
    highPassButton.addListener(this);
    highPassLabel.setFont(Font(11.f));
    addAndMakeVisible(&highPassLabel);
    highPassButton.setRadioGroupId(1);
    
    addAndMakeVisible(&bandPassButton);
    bandPassButton.addListener(this);
    bandPassLabel.setFont(Font(11.f));
    addAndMakeVisible(&bandPassLabel);
    bandPassButton.setRadioGroupId(1);
    
    switch (audio.filterType->getDefaultValue()) {
        case 0:
            lowPassButton.setToggleState(true, dontSendNotification);
            break;
        case 1:
            highPassButton.setToggleState(true, dontSendNotification);
            break;
        case 2:
            bandPassButton.setToggleState(true, dontSendNotification);
            break;
        default:
            break;
    }
    
    addAndMakeVisible(&envDepthSlider);
    envDepthSlider.setSliderStyle(Slider::Rotary);
    envDepthSlider.addListener(this);
    envDepthSlider.setRange(-100, 100, 1);
    envDepthSlider.setValue(audio.filterDepth->getDefaultValue() * 100);
    envDepthSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxBelow, false, 40, 20);
    envelopeLabel.setFont(Font(11.f));
    addAndMakeVisible(&envelopeLabel);
    
    addAndMakeVisible(&cutoffSlider);
    cutoffSlider.addListener(this);
    cutoffSlider.setSliderStyle(Slider::LinearVertical);
    cutoffSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    cutoffSlider.setRange(20, 20000, 1);
    cutoffSlider.setValue(audio.filterCutoff->getDefaultValue());
    cutoffSlider.setSkewFactor(0.5);
    cutoffLabel.setFont(Font (11.f));
    cutoffLabel.attachToComponent(&cutoffSlider, false);
    
    addAndMakeVisible(&qSlider);
    qSlider.addListener(this);
    qSlider.setSliderStyle(Slider::LinearVertical);
    qSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    qSlider.setRange(0.3, 20, 0.0001);
    qSlider.setValue(audio.filterQ->getDefaultValue());
    qSlider.setSkewFactor(0.3);
    qLabel.setFont(Font(11.f));
    qLabel.attachToComponent(&qSlider, false);
    qSlider.setValue(0.7);
    
    presetSection.setListener(this);
    
    filterSectionIdentifiers.add("filterQ");
    filterSectionIdentifiers.add("filterDepth");
    filterSectionIdentifiers.add("filterType");
    filterSectionIdentifiers.add("filterCutoff");
}

FilterSection::~FilterSection()
{
    
}

void FilterSection::paint(juce::Graphics &g)
{
    g.drawVerticalLine(0, 0, getHeight());
    g.drawHorizontalLine(0, 0, getWidth());
}

void FilterSection::resized()
{
    float leftSide = getWidth() / 3.f;
    float rightSide = getWidth() - leftSide;
    float leftComponentWidth = (leftSide - 60);
    float leftComponentHeight = (getHeight() - 100) / 4.f;
    float rightComponentWidth = (rightSide - 60) / 2.f;
    float rightComponentHeight = getHeight() - 40;

    lowPassButton.setBounds(20, 20, leftComponentWidth, leftComponentHeight);
    lowPassLabel.setBounds(lowPassButton.getRight(), lowPassButton.getY(), leftComponentWidth / 2, leftComponentHeight);
    highPassButton.setBounds(20, lowPassButton.getBottom() + 20, leftComponentWidth, leftComponentHeight);
    highPassLabel.setBounds(highPassButton.getRight(), highPassButton.getY(), leftComponentWidth / 2, leftComponentHeight);
    bandPassButton.setBounds(20, highPassButton.getBottom() + 20, leftComponentWidth, leftComponentHeight);
    bandPassLabel.setBounds(bandPassButton.getRight(), bandPassButton.getY(), leftComponentWidth / 2, leftComponentHeight);
    envDepthSlider.setBounds(20, bandPassButton.getBottom() + 25, leftComponentWidth, leftComponentHeight - 10);
    envelopeLabel.setBounds(envDepthSlider.getRight(), envDepthSlider.getY(), leftComponentWidth / 1.5, leftComponentHeight);
    
    cutoffSlider.setBounds(lowPassLabel.getRight() + 40, 20, rightComponentWidth, rightComponentHeight);
    qSlider.setBounds(cutoffSlider.getRight() + 20, 20, rightComponentWidth, rightComponentHeight);
    
    repaint();
}

void FilterSection::buttonClicked(juce::Button *button)
{
    if(button == &lowPassButton && lowPassButton.getToggleState())
    {
        ParameterPair param;
        
        param.identifier = Parameters::pFilterType;
        param.iVal = 0;
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(button == &highPassButton && highPassButton.getToggleState())
    {
        ParameterPair param;
        
        param.identifier = Parameters::pFilterType;
        param.iVal = 1;
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(button == &bandPassButton && bandPassButton.getToggleState())
    {
        ParameterPair param;
        
        param.identifier = Parameters::pFilterType;
        param.iVal = 2;
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
}

void FilterSection::sliderValueChanged(juce::Slider *slider)
{
    if(slider == &cutoffSlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pFilterCutoff;
        param.iVal = cutoffSlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &qSlider)
    {        
        ParameterPair param;
        
        param.identifier = Parameters::pFilterQ;
        param.fVal = qSlider.getValue();
        param.t = ParameterPair::Float;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &envDepthSlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pFilterDepth;
        param.fVal = envDepthSlider.getValue() / 100;
        param.t = ParameterPair::Float;
        audio.queue.addToFifo(&param, 1);
    }
}

void FilterSection::shouldUpdate(ValueTree currentPreset)
{
    ValueTree floats = currentPreset.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        Identifier id = floats.getPropertyName(i);
        if(id == filterSectionIdentifiers[0])
        {
            qSlider.setValue(floats.getProperty(id));
        }
        else if(id == filterSectionIdentifiers[1])
        {
            envDepthSlider.setValue((float)floats.getProperty(id) * 100);
        }
    }
    ValueTree ints = currentPreset.getChildWithName("INTS");
    for(int i = 0; i < ints.getNumProperties(); i++)
    {
        Identifier id = ints.getPropertyName(i);
        if(id == filterSectionIdentifiers[2])
        {
            if((int)ints.getProperty(id) == 0)
            {
                lowPassButton.setToggleState(true, dontSendNotification);
            }
            else if((int)ints.getProperty(id) == 1)
            {
                highPassButton.setToggleState(true, dontSendNotification);
            }
            else if((int)ints.getProperty(id) == 2)
            {
                bandPassButton.setToggleState(true, dontSendNotification);
            }
            
        }
        else if(id == filterSectionIdentifiers[3])
        {
            cutoffSlider.setValue(ints.getProperty(id));
        }
    }
}