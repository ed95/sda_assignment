//
//  SawOscillator.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 01/11/2015.
//
//

#include "SawOscillator.hpp"

SawVoice::SawVoice(Audio& audio_, int oscNum) : BaseOscillator(audio_, oscNum)
{
    
}

SawVoice::~SawVoice()
{
    
}

bool SawVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<SawSound*> (sound) != nullptr;
};

void SawVoice::setSTKFreq(float frequency)
{
    sawGen.setFrequency(frequency);
}

void SawVoice::resetSTKGen()
{
    sawGen.reset();
}

float SawVoice::renderWaveshape()
{
    return sawGen.tick();
}