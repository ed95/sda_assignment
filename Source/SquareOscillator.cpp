//
//  SquareOscillator.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 01/11/2015.
//
//

#include "SquareOscillator.hpp"

SquareVoice::SquareVoice(Audio& audio_, int oscNum) : BaseOscillator(audio_, oscNum)
{
    
}

SquareVoice::~SquareVoice()
{
    
}

bool SquareVoice::canPlaySound(juce::SynthesiserSound *sound)
{
    return dynamic_cast<SquareSound*> (sound) != nullptr;
}

void SquareVoice::setSTKFreq(float frequency)
{
    squareGen.setFrequency(frequency);
}

void SquareVoice::resetSTKGen()
{
    squareGen.reset();
}

float SquareVoice::renderWaveshape()
{
    return squareGen.tick();
}