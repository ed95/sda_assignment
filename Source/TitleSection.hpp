//
//  TitleSection.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 20/12/2015.
//
//

#ifndef TitleSection_hpp
#define TitleSection_hpp

/** 
 Title section UI
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class TitleSection : public Component
{
public:
    /** Constructor */
    TitleSection();
    
    /** Destructor */
    ~TitleSection();
    
    /** Overridden from Component */
    void paint(Graphics&) override;
    
    /** Overridden from Component */
    void resized() override;
    
private:
    Label synthTitle;
};
#endif /* TitleSection_hpp */
