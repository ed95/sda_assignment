//
//  ArpeggiatorSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 29/11/2015.
//
//

#include "ArpeggiatorSection.hpp"

ArpeggiatorSection::ArpeggiatorSection(Audio& audio_, PresetSection& presetSection_) : audio(audio_), presetSection(presetSection_), arpLabel("", "ON"), latchLabel("", "LATCH"), arpBPMLabel("", "ARPEGGIATOR BPM:"), arpPatternLabel("", "ARPEGGIATOR PATTERN:"), arpResolutionLabel("", "ARPEGGIATOR RESOLUTION:"), swingLabel("", "SWING"), velocityToggleLabel("", "VEL"), velocityLabel("", "AMOUNT"), gateLabel("", "GATE")
{
    addAndMakeVisible(&arpToggle);
    arpToggle.addListener(this);
    arpToggle.setToggleState(audio.arpeggiatorState->getDefaultValue(), dontSendNotification);
    arpLabel.setFont(Font(11.f));
    arpLabel.setJustificationType(Justification::centred);
    arpLabel.attachToComponent(&arpToggle, false);
    
    addAndMakeVisible(&arpLatchToggle);
    arpLatchToggle.addListener(this);
    arpLatchToggle.setToggleState(audio.arpeggiatorLatchState->getDefaultValue(), dontSendNotification);
    latchLabel.setFont(Font(11.f));
    latchLabel.setJustificationType(Justification::centred);
    latchLabel.attachToComponent(&arpLatchToggle, false);
    
    addAndMakeVisible(&arpBPMSlider);
    arpBPMSlider.setSliderStyle(Slider::Rotary);
    arpBPMSlider.addListener(this);
    arpBPMSlider.setRange(20, 250, 1);
    arpBPMSlider.setValue(audio.arpeggiatorBPM->getDefaultValue());
    arpBPMLabel.attachToComponent(&arpBPMSlider, false);
    arpBPMLabel.setJustificationType(Justification::centred);
    arpBPMLabel.setFont(Font(11.0f));
    
    addAndMakeVisible(&arpPatternSelector);
    arpPatternSelector.addItem("Ascending", 1);
    arpPatternSelector.addItem("Descending", 2);
    arpPatternSelector.addItem("Random", 3);
    arpPatternSelector.addListener(this);
    arpPatternSelector.setSelectedId(audio.arpeggiatorPattern->getDefaultValue());
    arpPatternLabel.attachToComponent(&arpPatternSelector, false);
    arpPatternLabel.setJustificationType(Justification::centred);
    arpPatternLabel.setFont(Font(11.f));
    
    for(int i = 0; i < 8; i++)
    {
        addAndMakeVisible(&arpButton[i]);
        arpButton[i].addListener(this);
    }
    
    addAndMakeVisible(&arpResolutionSlider);
    arpResolutionSlider.setSliderStyle(Slider::Rotary);
    arpResolutionSlider.addListener(this);
    arpResolutionSlider.setRange(4, 24, 2);
    arpResolutionSlider.setValue(audio.arpeggiatorResolution->getDefaultValue());
    arpResolutionLabel.attachToComponent(&arpResolutionSlider, false);
    arpResolutionLabel.setJustificationType(Justification::centred);
    arpResolutionLabel.setFont(Font(11.0f));
    
    addAndMakeVisible(&gateSlider);
    gateSlider.setSliderStyle(Slider::Rotary);
    gateSlider.addListener(this);
    gateSlider.setRange(0, 1, 0.01);
    gateSlider.setValue(audio.arpeggiatorGate->getDefaultValue());
    gateLabel.setFont(Font(11.f));
    gateLabel.setJustificationType(Justification::centred);
    gateLabel.attachToComponent(&gateSlider, false);
    
    addAndMakeVisible(&swingSlider);
    swingSlider.setSliderStyle(Slider::Rotary);
    swingSlider.addListener(this);
    swingSlider.setRange(0, 100, 1);
    swingSlider.setValue(audio.arpeggiatorSwing->getDefaultValue());
    swingLabel.setFont(Font(11.f));
    swingLabel.setJustificationType(Justification::centred);
    swingLabel.attachToComponent(&swingSlider, false);
    
    addAndMakeVisible(&velocityToggle);
    velocityToggle.addListener(this);
    velocityToggle.setToggleState(audio.arpeggiatorVelocityState->getDefaultValue(), dontSendNotification);
    velocityToggleLabel.setFont(Font(11.f));
    velocityToggleLabel.setJustificationType(Justification::centred);
    velocityToggleLabel.attachToComponent(&velocityToggle, false);
    
    addAndMakeVisible(&velocitySlider);
    velocitySlider.setSliderStyle(Slider::Rotary);
    velocitySlider.addListener(this);
    velocitySlider.setRange(0, 127, 1);
    velocitySlider.setValue(audio.arpeggiatorVelocity->getDefaultValue());
    velocityLabel.setFont(Font(11.f));
    velocityLabel.setJustificationType(Justification::centred);
    velocityLabel.attachToComponent(&velocitySlider, false);
    
    controlsEnabled(audio.arpeggiatorState->getDefaultValue());

    audio.getArpeggiator().setListener(this);
    presetSection.setListener(this);
    
    arpeggiatorSectionIdentifiers.add("arpeggiatorGate");
    arpeggiatorSectionIdentifiers.add("arpeggiatorPattern");
    arpeggiatorSectionIdentifiers.add("arpeggiatorBPM");
    arpeggiatorSectionIdentifiers.add("arpeggiatorResolution");
    arpeggiatorSectionIdentifiers.add("arpeggiatorSwing");
    arpeggiatorSectionIdentifiers.add("arpeggiatorVelocity");
    arpeggiatorSectionIdentifiers.add("beatActive1");
    arpeggiatorSectionIdentifiers.add("beatActive2");
    arpeggiatorSectionIdentifiers.add("beatActive3");
    arpeggiatorSectionIdentifiers.add("beatActive4");
    arpeggiatorSectionIdentifiers.add("beatActive5");
    arpeggiatorSectionIdentifiers.add("beatActive6");
    arpeggiatorSectionIdentifiers.add("beatActive7");
    arpeggiatorSectionIdentifiers.add("beatActive8");
    arpeggiatorSectionIdentifiers.add("arpeggiatorState");
    arpeggiatorSectionIdentifiers.add("arpeggiatorLatchState");
    arpeggiatorSectionIdentifiers.add("arpeggiatorVelocityState");
}

ArpeggiatorSection::~ArpeggiatorSection()
{
    
}

void ArpeggiatorSection::paint(Graphics& g)
{

}

void ArpeggiatorSection::resized()
{
    int componentWidth = ((getWidth() - 80) / 5);
    int componentHeight = getHeight() / 10;
    
    arpToggle.setBounds(20, 20, componentWidth / 2, componentHeight);
    arpLatchToggle.setBounds(20, arpToggle.getBottom() + 20, componentWidth / 2, componentHeight);
    velocityToggle.setBounds(arpToggle.getRight() + (componentWidth / 4), 20, componentWidth / 2, componentHeight);
    velocitySlider.setBounds(arpToggle.getRight(), velocityToggle.getBottom() + 20, componentWidth, componentHeight);
    
    arpBPMSlider.setBounds(velocitySlider.getRight() + 20, 20, (componentWidth * 2) - 40, componentHeight);
    arpResolutionSlider.setBounds(arpBPMSlider.getX(), arpBPMSlider.getBottom() + 20, (componentWidth * 2) - 40, componentHeight);
    
    arpPatternSelector.setBounds(arpBPMSlider.getRight() + 20, 20, componentWidth * 2, componentHeight);
    
    gateSlider.setBounds(arpPatternSelector.getX(), arpPatternSelector.getBottom() + 20, componentWidth, componentHeight);
    swingSlider.setBounds(gateSlider.getRight(), gateSlider.getY(), componentWidth, componentHeight);
    
    int buttonWidth = (getWidth() - 40) / 8;
    int buttonHeight = componentHeight;
    
    for(int i = 0; i < 8; i++)
    {
        arpButton[i].setBounds(20 + (buttonWidth * i), arpResolutionSlider.getBottom() + 40, buttonWidth, buttonHeight);
    }
}

void ArpeggiatorSection::buttonClicked (Button* button)
{
    if(button == &arpToggle)
    {
        ParameterPair param;

        bool isOn = arpToggle.getToggleState();
        if(isOn)
        {
            audio.getArpeggiator().reset();
        }
        else
        {            
            param.identifier = Parameters::pArpeggiatorException;
            param.bVal = true;
            param.t = ParameterPair::Bool;
            audio.queue.addToFifo(&param, 1);
        }
        
        controlsEnabled(isOn);
        
        param.identifier = Parameters::pArpeggiatorState;
        param.bVal = isOn;
        param.t = ParameterPair::Bool;
        audio.queue.addToFifo(&param, 1);
    }
    else if(button == &arpLatchToggle)
    {
        ParameterPair param;
        
        bool latchOn = arpLatchToggle.getToggleState();
        if(!latchOn)
        {
            param.identifier = Parameters::pArpeggiatorLatchException;
            param.bVal = true;
            param.t = ParameterPair::Bool;
            audio.queue.addToFifo(&param, 1);
        }
        else
        {
            audio.getArpeggiator().clearNotes = true;
        }
        
        param.identifier = Parameters::pArpeggiatorLatchState;
        param.bVal = true;
        param.t = ParameterPair::Bool;
        audio.queue.addToFifo(&param, 1);
    }
    else if(button == &velocityToggle)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pArpeggiatorVelocityState;
        param.bVal = velocityToggle.getToggleState();
        param.t = ParameterPair::Bool;
        audio.queue.addToFifo(&param, 1);
        
        velocitySlider.setEnabled(velocityToggle.getToggleState());
    }
    else
    {
        for(int i = 0; i < 8; i++)
        {
            if(button == &arpButton[i])
            {
                ParameterPair param;
                
                bool state = !arpButton[i].getState();
                arpButton[i].setState(state);
                
                param.identifier = (Parameters)(Parameters::pBeatActive0 + i);
                param.bVal = state;
                param.t = ParameterPair::Bool;
                audio.queue.addToFifo(&param, 1);
            }
        }
    }
}

void ArpeggiatorSection::sliderValueChanged (Slider* slider)
{
    if(slider == &arpBPMSlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pArpeggiatorBPM;
        param.iVal = arpBPMSlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &arpResolutionSlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pArpeggiatorResolution;
        param.iVal = arpResolutionSlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &gateSlider)
    {
        ParameterPair param;

        param.identifier = Parameters::pArpeggiatorGate;
        param.fVal = gateSlider.getValue();
        param.t = ParameterPair::Float;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &swingSlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pArpeggiatorSwing;
        param.iVal = swingSlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &velocitySlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pArpeggiatorVelocity;
        param.iVal = velocitySlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
}

void ArpeggiatorSection::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if(comboBoxThatHasChanged == &arpPatternSelector)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pArpeggiatorPattern;
        param.iVal = arpPatternSelector.getSelectedId();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
        
        audio.getArpeggiator().shouldUpdate = true;
    }
}

void ArpeggiatorSection::shouldFlash(int buttonNum)
{
    MessageManagerLock mml(Thread::getCurrentThread());
    
    if(!mml.lockWasGained())
        return;
    
    arpButton[buttonNum].flash();
}

void ArpeggiatorSection::shouldUpdate(ValueTree currentPreset)
{
    ValueTree floats = currentPreset.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        Identifier id = floats.getPropertyName(i);
        if(id == arpeggiatorSectionIdentifiers[0])
        {
            gateSlider.setValue(floats.getProperty(id));
        }
    }
    ValueTree ints = currentPreset.getChildWithName("INTS");
    for(int i = 0; i < ints.getNumProperties(); i++)
    {
        Identifier id = ints.getPropertyName(i);
        if(id == arpeggiatorSectionIdentifiers[1])
        {
            arpPatternSelector.setSelectedId(ints.getProperty(id));
        }
        else if(id == arpeggiatorSectionIdentifiers[2])
        {
            arpBPMSlider.setValue(ints.getProperty(id));
        }
        else if(id == arpeggiatorSectionIdentifiers[3])
        {
            arpResolutionSlider.setValue(ints.getProperty(id));
        }
        else if(id == arpeggiatorSectionIdentifiers[4])
        {
            swingSlider.setValue(ints.getProperty(id));
        }
        else if(id == arpeggiatorSectionIdentifiers[5])
        {
            velocitySlider.setValue(ints.getProperty(id));
        }
    }
    ValueTree bools = currentPreset.getChildWithName("BOOLS");
    for(int i = 0; i < bools.getNumProperties(); i++)
    {
        Identifier id = bools.getPropertyName(i);
        if(id.toString().contains("beatActive"))
        {
            for(int j = 6; j < 14; j++)
            {
                if(id == arpeggiatorSectionIdentifiers[j])
                    arpButton[j - 6].setState(bools.getProperty(arpeggiatorSectionIdentifiers[j]));
            }
        }
        else if(id == arpeggiatorSectionIdentifiers[14])
        {
            arpToggle.setToggleState(bools.getProperty(id), dontSendNotification);
            controlsEnabled(bools.getProperty(id));
        }
        else if(id == arpeggiatorSectionIdentifiers[15])
        {
            arpLatchToggle.setToggleState(bools.getProperty(id), dontSendNotification);
        }
        else if(id == arpeggiatorSectionIdentifiers[16])
        {
            velocityToggle.setToggleState(bools.getProperty(id), dontSendNotification);
        }
    }
}

void ArpeggiatorSection::controlsEnabled(bool enabled)
{
    arpBPMSlider.setEnabled(enabled);
    arpPatternSelector.setEnabled(enabled);
    arpResolutionSlider.setEnabled(enabled);
    arpLatchToggle.setEnabled(enabled);
    for(int i = 0; i < 8; i++)
        arpButton[i].setEnabled(enabled);
    swingSlider.setEnabled(enabled);
    velocityToggle.setEnabled(enabled);
    velocitySlider.setEnabled(enabled);
    gateSlider.setEnabled(enabled);
}