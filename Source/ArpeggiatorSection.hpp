//
//  ArpeggiatorSection.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 29/11/2015.
//
//

#ifndef ArpeggiatorSection_hpp
#define ArpeggiatorSection_hpp

/**
 Arpeggiator section UI
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "ArpeggiatorButton.hpp"
#include "Arpeggiator.hpp"
#include "PushButton.hpp"
#include "PresetSection.hpp"

class ArpeggiatorSection : public Component,
                           public Button::Listener,
                           public Slider::Listener,
                           public ComboBox::Listener,
                           public Arpeggiator::Listener,
                           public PresetSection::Listener
{
public:
    /** Constructor.
     @param audio_ A reference to the Audio class @see Audio
     @param presetSection_ A reference to the PresetSection object so this class can listen to it @see PresetSection
     */
    ArpeggiatorSection(Audio& audio_, PresetSection& presetSection_);
    
    /** Destructor. */
    ~ArpeggiatorSection();
    
    /** Overridden from Component. */
    void paint(Graphics&) override;
    
    /** Overridden from Component. */
    void resized() override;
    
    /** Button listener. 
        @param button The button that has changed.
     */
    void buttonClicked (Button* button) override;
    
    /** Slider listener. 
        @param slider The slider that has changed.
     */
    void sliderValueChanged (Slider* slider) override;
    
    /** ComboBox listener.
        @param comboBoxThatHasChanged The combo box that has changed.
    */
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    /** Arpeggiator listener. 
        @param buttonNum The arpeggiator button number that should flash @see ArpeggiatorButton
     */
    void shouldFlash (int buttonNum) override;
    
    /** PresetSection listener.
     @param currentPreset A ValueTree object containing the selected preset so this section can update its UI
     */
    void shouldUpdate(ValueTree currentPreset) override;
    
    /** Enable or disable the controls.
        @param enabled True if enabled, false if disabled
     */
    void controlsEnabled(bool enabled);
    
private:
    Audio& audio; /// A reference to the Audio class
    PresetSection& presetSection; /// A reference to the PresetSection class
    Slider arpBPMSlider, arpResolutionSlider, swingSlider, velocitySlider, gateSlider;
    Label arpLabel, latchLabel, arpBPMLabel, arpPatternLabel, arpResolutionLabel, swingLabel, velocityToggleLabel, velocityLabel, gateLabel;
    PushButton arpToggle, arpLatchToggle, velocityToggle;
    ComboBox arpPatternSelector;
    ArpeggiatorButton arpButton[8]; /// An array of arpeggiator buttons
    Array<Identifier> arpeggiatorSectionIdentifiers; /// An array of this sections identifiers so it can update from the value tree
};
#endif /* ArpeggiatorSection_hpp */
