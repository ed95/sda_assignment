//
//  LPF.cpp
//  pluginTest
//
//  Created by Edward Davies on 16/11/2015.
//
//

#include "LPF.hpp"

LPF::LPF(float sampleRate)
{
    fSR = sampleRate;
    qVal = 0.7;
    setCutoff(20000.f);
}

LPF::~LPF()
{
    
}

void LPF::setQ(float q)
{
    qVal = q;
}

void LPF::setCutoff(float frequency)
{
    float kVal = tan(M_PI * (frequency / fSR));
    float norm = 1.f / (1 + (kVal / qVal) + (kVal * kVal));
    float b0 = kVal * kVal * norm;
    
    filter.setB0(b0);
    filter.setB1(2 * b0);
    filter.setB2(b0);
    
    filter.setA1(2 * (kVal * kVal - 1) * norm);
    filter.setA2((1 - (kVal / qVal) + (kVal * kVal)) * norm);
}

float LPF::outputFiltered(float inputSample)
{
    return filter.tick(inputSample);
}