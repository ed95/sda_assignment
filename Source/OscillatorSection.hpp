//
//  OscillatorSection.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#ifndef OscillatorSection_hpp
#define OscillatorSection_hpp

/**
 Oscillator section UI
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "PushButton.hpp"
#include "LEDButton.hpp"
#include "Audio.hpp"
#include "PresetSection.hpp"
#define NUMOSCS 3

class OscillatorSection : public Component,
                          public Button::Listener,
                          public Slider::Listener,
                          public PresetSection::Listener
{
public:
    /** Constructor.
     @param audio_ A reference to the Audio class @see Audio
     @param num This oscillator instance's number
     @param presetSection_ A reference to the PresetSection object so this class can listen to it @see PresetSection
     */
    OscillatorSection(Audio& audio_, int num, PresetSection& presetSection_);
    
    /** Destructor. */
    ~OscillatorSection();
    
    /** Overridden from Component. */
    void paint(Graphics&) override;
    
    /** Overridden from Component. */
    void resized() override;
    
    /** Button listener.
     @param button The button that has changed.
     */
    void buttonClicked (Button* button) override;
    
    /** Slider listener.
     @param slider The slider that has changed.
     */
    void sliderValueChanged (Slider* slider) override;
    
    /** PresetSection listener.
     @param currentPreset A ValueTree object containing the selected preset so this section can update its UI
     */
    void shouldUpdate(ValueTree currentPreset) override;
    
    /** Returns the argument component's X position.
        @return The X position
        @param componentNum The component position to get
     */
    int getComponentX(int componentNum);
    
    /** Returns the argument component's width.
         @return The width
         @param componentNum The component position to get
     */
    int getComponentWidth(int componentNum);
    
private:
    Audio& audio; /// A reference to the Audio class
    PresetSection& presetSection; /// A reference to the PresetSection class
    Slider oscOctaveSlider, oscDetuneSlider, oscAmpSlider;
    LEDButton oscWaveshapeToggle[4];
    PushButton oscOnOffToggle;
    Image waveshapeImage[4];
    ImageComponent oscWaveshapeImageComponent[4];
    
    int oscNum; /// This oscillator instance's number
    
    Array<Identifier> oscillatorSectionIdentifiers; /// An array of this sections identifiers so it can update from the value tree
};

#endif /* OscillatorSection_hpp */
