//
//  PresetSection.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 20/12/2015.
//
//

#ifndef PresetSection_hpp
#define PresetSection_hpp

/**
 Preset section UI
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "LEDButton.hpp"
#include "PresetStates.hpp"

class PresetSection : public Component,
                      public Button::Listener
{
public:
    /** Constructor.
        @param audio_ A reference to the Audio class @see Audio
     */
    PresetSection(Audio& audio_);
    
    /** Destructor */
    ~PresetSection();
    
    /** Overridden from Component. */
    void paint(Graphics&) override;
    
    /** Overridden from Component. */
    void resized() override;
    
    /** Button listener.
     @param button The button that has changed.
     */
    void buttonClicked (Button* button) override;

    /**
     Listener class
     */
    class Listener
    {
    public:
        /** Destructor */
        virtual ~Listener(){}
        
        /** Called when a new preset is chosen and the other UI elements need to update.
            @param currentPreset A ValueTree object containing the selected preset so this section can update its UI
         */
        virtual void shouldUpdate(ValueTree currentPreset) = 0;
    };
    
    /** Sets the listener.
        @param newListener A pointer to the new listener
     */
    void setListener (Listener* newListener);
    
private:
    Label numberLabels[8];
    Label letterLabels[2];
    Label selectLabel, writeLabel, initLabel, presetLabel;
    
    LEDButton presetToggles[2][8];
    LEDButton selectButton, writeButton, initButton;
    
    bool selectMode, writeMode, initMode;
    int previousButton;
    
    PresetStates presets; /// A class handling the preset states @see PresetStates
    ListenerList<Listener> presetSectionListeners; /// List containing the listeners to this class
};

#endif /* PresetSection_hpp */
