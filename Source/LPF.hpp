//
//  LPF.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#ifndef LPF_hpp
#define LPF_hpp

/**
 Lowpass filter
 */

#include <stdio.h>
#include "BiQuadFilter.hpp"

class LPF : public BiQuadFilter
{
public:
    /** Constructor.
     @param audio_ A reference to the Audio class @see Audio
     */
    LPF(Audio& audio_);
    
    /** Destructor. */
    ~LPF();
    
    /* Inherited from parent class. Sets the filter cutoff.
     @param frequency The desired cutoff frequency
     */
    void setCutoff(float frequency) override;
    
private:
};

#endif /* LPF_hpp */
