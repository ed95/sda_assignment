//
//  TitleSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 20/12/2015.
//
//

#include "TitleSection.hpp"

TitleSection::TitleSection() : synthTitle("", "SDA SYNTH")
{
    addAndMakeVisible(&synthTitle);
    synthTitle.setJustificationType(Justification::centred);
    synthTitle.setFont(Font(48.0f));
}

TitleSection::~TitleSection()
{
    
}

void TitleSection::paint(Graphics &)
{
    
}

void TitleSection::resized()
{
    synthTitle.setBounds(20, 0, getWidth(), getHeight());
}