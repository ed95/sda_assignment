//
//  ADSRSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#include "ADSRSection.hpp"

ADSRSection::ADSRSection(Audio& audio_, int appliesTo, PresetSection& presetSection_) : audio (audio_), presetSection(presetSection_), attackLabel("", "A"), decayLabel("", "D"), sustainLabel("", "S"), releaseLabel("", "R"), envDestination(appliesTo)
{
    addAndMakeVisible(&attackSlider);
    attackSlider.addListener(this);
    attackSlider.setSliderStyle(Slider::LinearVertical);
    attackSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    attackSlider.setRange(0.01, 5, 0.001);
    attackSlider.setValue((envDestination == 0) ? audio.aAttack->getDefaultValue() : audio.fAttack->getDefaultValue());
    attackSlider.setSkewFactor(0.5);
    attackLabel.setFont(Font (11.f));
    attackLabel.setJustificationType(Justification::centred);
    attackLabel.attachToComponent(&attackSlider, false);
    
    addAndMakeVisible(&decaySlider);
    decaySlider.addListener(this);
    decaySlider.setSliderStyle(Slider::LinearVertical);
    decaySlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    decaySlider.setRange(0.01, 5, 0.001);
    decaySlider.setValue((envDestination == 0) ? audio.aDecay->getDefaultValue() : audio.fDecay->getDefaultValue());
    decaySlider.setSkewFactor(0.5);
    decayLabel.setFont(Font (11.f));
    decayLabel.setJustificationType(Justification::centred);
    decayLabel.attachToComponent(&decaySlider, false);
    
    addAndMakeVisible(&sustainSlider);
    sustainSlider.addListener(this);
    sustainSlider.setSliderStyle(Slider::LinearVertical);
    sustainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    sustainSlider.setRange(0, (envDestination == 0) ? 1 : 100, (envDestination == 0) ? 0.01 : 1);
    sustainSlider.setValue((envDestination == 0) ? audio.aSustain->getDefaultValue() : audio.fSustain->getDefaultValue() * 100);
    sustainLabel.setFont(Font (11.f));
    sustainLabel.setJustificationType(Justification::centred);
    sustainLabel.attachToComponent(&sustainSlider, false);
    
    addAndMakeVisible(&releaseSlider);
    releaseSlider.addListener(this);
    releaseSlider.setSliderStyle(Slider::LinearVertical);
    releaseSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 20);
    releaseSlider.setRange(0.01, 5, 0.001);
    releaseSlider.setValue((envDestination == 0) ? audio.aRelease->getDefaultValue() : audio.fRelease->getDefaultValue());
    releaseSlider.setSkewFactor(0.5);
    releaseLabel.setFont(Font (11.f));
    releaseLabel.setJustificationType(Justification::centred);
    releaseLabel.attachToComponent(&releaseSlider, false);
    
    adsrSectionIdentifiers.add("aAttack");
    adsrSectionIdentifiers.add("aDecay");
    adsrSectionIdentifiers.add("aSustain");
    adsrSectionIdentifiers.add("aRelease");
    adsrSectionIdentifiers.add("fAttack");
    adsrSectionIdentifiers.add("fDecay");
    adsrSectionIdentifiers.add("fSustain");
    adsrSectionIdentifiers.add("fRelease");
    
    presetSection.setListener(this);
}

ADSRSection::~ADSRSection()
{
    
}

void ADSRSection::paint(juce::Graphics &g)
{
    g.drawVerticalLine(0, 0, getHeight());
}

void ADSRSection::resized()
{
    float compWidth = (getWidth() - 100) / 4.f;
    float compHeight = getHeight() - 40;
    
    attackSlider.setBounds(20, 20, compWidth, compHeight);
    decaySlider.setBounds(attackSlider.getRight() + 20, 20, compWidth, compHeight);
    sustainSlider.setBounds(decaySlider.getRight() + 20, 20, compWidth, compHeight);
    releaseSlider.setBounds(sustainSlider.getRight() + 20, 20, compWidth, compHeight);
    
    repaint();
}

void ADSRSection::sliderValueChanged(juce::Slider *slider)
{
    ParameterPair param;
    if(envDestination == 0)
    {
        if(slider == &attackSlider)
        {
            param.identifier = Parameters::pAAttack;
            param.fVal = attackSlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
        else if(slider == &decaySlider)
        {
            param.identifier = Parameters::pADecay;
            param.fVal = decaySlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
        else if(slider == &sustainSlider)
        {
            param.identifier = Parameters::pASustain;
            param.fVal = sustainSlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
        else if(slider == &releaseSlider)
        {
            param.identifier = Parameters::pARelease;
            param.fVal = releaseSlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
    }
    else if(envDestination == 1)
    {
        if(slider == &attackSlider)
        {
            param.identifier = Parameters::pFAttack;
            param.fVal = attackSlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
        else if(slider == &decaySlider)
        {
            param.identifier = Parameters::pFDecay;
            param.fVal = decaySlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
        else if(slider == &sustainSlider)
        {
            param.identifier = Parameters::pFSustain;
            param.fVal = sustainSlider.getValue() / 100;
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
        else if(slider == &releaseSlider)
        {
            param.identifier = Parameters::pFRelease;
            param.fVal = releaseSlider.getValue();
            param.t = ParameterPair::Float;
            audio.queue.addToFifo(&param, 1);
        }
    }
}

void ADSRSection::shouldUpdate(ValueTree currentPreset)
{
    ValueTree floats = currentPreset.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        Identifier id = floats.getPropertyName(i);
        if(id == (envDestination == 0 ? adsrSectionIdentifiers[0] : adsrSectionIdentifiers[4]))
        {
            attackSlider.setValue(floats.getProperty(id));
        }
        else if(id == (envDestination == 0 ? adsrSectionIdentifiers[1] : adsrSectionIdentifiers[5]))
        {
            decaySlider.setValue(floats.getProperty(id));
        }
        else if(id == (envDestination == 0 ? adsrSectionIdentifiers[2] : adsrSectionIdentifiers[6]))
        {
            float val = floats.getProperty(id);
            envDestination == 0 ? val = val : val *= 100;
            sustainSlider.setValue(val);
        }
        else if(id == (envDestination == 0 ? adsrSectionIdentifiers[3] : adsrSectionIdentifiers[7]))
        {
            releaseSlider.setValue(floats.getProperty(id));
        }
    }
}