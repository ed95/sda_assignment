//
//  OscillatorSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#include "OscillatorSection.hpp"

OscillatorSection::OscillatorSection(Audio& audio_, int num, PresetSection& presetSection_) : audio(audio_), presetSection(presetSection_), oscNum(num)
{
    addAndMakeVisible(&oscOnOffToggle);
    oscOnOffToggle.setToggleState(audio.oscEnabled[oscNum]->getDefaultValue(), dontSendNotification);
    oscOnOffToggle.addListener(this);
    
    File location = location.getSpecialLocation(File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile("images");
    
    File sineImagePath = File(location.getChildFile("oscillator_sine.gif"));
    File squareImagePath = File(location.getChildFile("oscillator_square.gif"));
    File sawImagePath = File(location.getChildFile("oscillator_saw.gif"));
    File noiseImagePath = File(location.getChildFile("oscillator_noise.gif"));
    
    waveshapeImage[0] = ImageFileFormat::loadFrom(sineImagePath);
    waveshapeImage[1] = ImageFileFormat::loadFrom(squareImagePath);
    waveshapeImage[2] = ImageFileFormat::loadFrom(sawImagePath);
    waveshapeImage[3] = ImageFileFormat::loadFrom(noiseImagePath);
    for(int i = 0; i < 4; i++)
    {
        addAndMakeVisible(&oscWaveshapeToggle[i]);
        oscWaveshapeToggle[i].addListener(this);
        oscWaveshapeToggle[i].setRadioGroupId(1);
        
        addAndMakeVisible(&oscWaveshapeImageComponent[i]);
        oscWaveshapeImageComponent[i].setImage(waveshapeImage[i]);
    }
    
    oscWaveshapeToggle[audio.oscWaveshape[oscNum]->getDefaultValue()].setToggleState(true, dontSendNotification);

    addAndMakeVisible(&oscOctaveSlider);
    oscOctaveSlider.setSliderStyle(Slider::IncDecButtons);
    oscOctaveSlider.addListener(this);
    oscOctaveSlider.setRange(-3, 3, 1);
    oscOctaveSlider.setValue(audio.octaveShift[oscNum]->getDefaultValue());
    
    addAndMakeVisible(&oscDetuneSlider);
    oscDetuneSlider.setSliderStyle(Slider::Rotary);
    oscDetuneSlider.addListener(this);
    oscDetuneSlider.setRange(-120, 120, 1);
    oscDetuneSlider.setValue(audio.detuneAmount[oscNum]->getDefaultValue());
    oscDetuneSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::TextBoxBelow, false, 40, 20);
    
    addAndMakeVisible(&oscAmpSlider);
    oscAmpSlider.setSliderStyle(Slider::Rotary);
    oscAmpSlider.addListener(this);
    oscAmpSlider.setRange(0.0, 1.f / NUMOSCS, 0.01);
    oscAmpSlider.setValue(audio.oscLevel[oscNum]->getDefaultValue());
    oscAmpSlider.setTextBoxStyle(Slider::TextEntryBoxPosition::NoTextBox, false, 0, 0);
    
    for(int i = 0; i < 4; i++)
    {
        if(oscWaveshapeToggle[i].getToggleState())
        {
            audio.setWaveshape(oscNum, i);
        }
    }
    
    presetSection.setListener(this);
    
    String l = "oscLevel";
    String s = "octaveShift";
    String d = "detuneAmount";
    String w = "oscWaveshape";
    String e = "oscEnabled";
    oscillatorSectionIdentifiers.add(l + String(oscNum + 1));
    oscillatorSectionIdentifiers.add(s + String(oscNum + 1));
    oscillatorSectionIdentifiers.add(d + String(oscNum + 1));
    oscillatorSectionIdentifiers.add(w + String(oscNum + 1));
    oscillatorSectionIdentifiers.add(e + String(oscNum + 1));
}

OscillatorSection::~OscillatorSection()
{
    
}

void OscillatorSection::paint(juce::Graphics &g)
{
    
}

void OscillatorSection::resized()
{
    float componentWidth = (getWidth() - 120) / 6;
    
    oscOnOffToggle.setBounds(20, 10, componentWidth - 40, getHeight() - 20);
    for(int i = 0; i < 4; i++)
    {
        oscWaveshapeToggle[i].setBounds(oscOnOffToggle.getRight() + 20 + (i * (componentWidth / 2)), getHeight() / 2, componentWidth / 2, getHeight() / 2);
        oscWaveshapeImageComponent[i].setBounds(oscWaveshapeToggle[i].getX(), 0, componentWidth / 2, getHeight() / 2);
    }
    oscOctaveSlider.setBounds(oscWaveshapeToggle[3].getRight() + 20, 0, componentWidth, getHeight());
    oscDetuneSlider.setBounds(oscOctaveSlider.getRight() + 20, oscOctaveSlider.getY(), componentWidth, getHeight());
    oscAmpSlider.setBounds(oscDetuneSlider.getRight() + 20, oscOctaveSlider.getY(), componentWidth, getHeight());
}

void OscillatorSection::buttonClicked(juce::Button *button)
{
    if(button == &oscOnOffToggle)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)(Parameters::pOscEnabled0 + oscNum);
        param.bVal = oscOnOffToggle.getToggleState();
        param.t = ParameterPair::Bool;
        audio.queue.addToFifo(&param, 1);
    }
    else
    {
        for(int i = 0; i < 4; i++)
        {
            if(button == &oscWaveshapeToggle[i])
            {
                ParameterPair param;
                if(oscWaveshapeToggle[i].getToggleState())
                {
                    audio.setWaveshape(oscNum, i);
                    param.identifier = (Parameters)(Parameters::pOscWaveshape0 + oscNum);
                    param.iVal = i;
                    param.t = ParameterPair::Int;
                    audio.queue.addToFifo(&param, 1);
                }
                
                //if noise
                oscDetuneSlider.setEnabled((i == 3) ? false : true);
                oscOctaveSlider.setEnabled((i == 3) ? false : true);
            }
        }
    }
}

void OscillatorSection::sliderValueChanged(juce::Slider *slider)
{
    if(slider == &oscAmpSlider)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)(Parameters::pOscLevel0 + oscNum);
        param.fVal = oscAmpSlider.getValue();
        param.t = ParameterPair::Float;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &oscOctaveSlider)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)(Parameters::pOctaveShift0 + oscNum);
        param.iVal = oscOctaveSlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    else if(slider == &oscDetuneSlider)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)(Parameters::pDetuneAmount0 + oscNum);
        param.iVal = oscDetuneSlider.getValue();
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
}

int OscillatorSection::getComponentX(int componentNum)
{
    switch (componentNum) {
        case 0:
            return oscOnOffToggle.getX();
            break;
        case 1:
            return oscWaveshapeToggle[0].getX();
            break;
        case 2:
            return oscWaveshapeToggle[1].getX();
            break;
        case 3:
            return oscWaveshapeToggle[2].getX();
            break;
        case 4:
            return oscWaveshapeToggle[3].getX();
            break;
        case 5:
            return oscOctaveSlider.getX();
            break;
        case 6:
            return oscDetuneSlider.getX();
            break;
        case 7:
            return oscAmpSlider.getX();
            break;
        default:
            return 0;
            break;
    }
}

int OscillatorSection::getComponentWidth(int componentNum)
{
    switch (componentNum) {
        case 0:
            return oscOnOffToggle.getWidth();
            break;
        case 1:
            return oscWaveshapeToggle[0].getWidth();
            break;
        case 2:
            return oscWaveshapeToggle[1].getWidth();
            break;
        case 3:
            return oscWaveshapeToggle[2].getWidth();
            break;
        case 4:
            return oscWaveshapeToggle[3].getWidth();
            break;
        case 5:
            return oscOctaveSlider.getWidth();
            break;
        case 6:
            return oscDetuneSlider.getWidth();
            break;
        case 7:
            return oscAmpSlider.getWidth();
            break;
        default:
            return 0;
            break;
    }
}

void OscillatorSection::shouldUpdate(ValueTree currentPreset)
{
    ValueTree floats = currentPreset.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        Identifier id = floats.getPropertyName(i);
        if(id == oscillatorSectionIdentifiers[0])
        {
            oscAmpSlider.setValue(floats.getProperty(id));
        }
    }
    ValueTree ints = currentPreset.getChildWithName("INTS");
    for(int i = 0; i < ints.getNumProperties(); i++)
    {
        Identifier id = ints.getPropertyName(i);
        if(id == oscillatorSectionIdentifiers[1])
        {
            oscOctaveSlider.setValue(ints.getProperty(id));
        }
        else if(id == oscillatorSectionIdentifiers[2])
        {
            oscDetuneSlider.setValue(ints.getProperty(id));
        }
        else if(id == oscillatorSectionIdentifiers[3])
        {
            oscWaveshapeToggle[(int)ints.getProperty(id)].setToggleState(true, sendNotification);
        }
    }
    ValueTree bools = currentPreset.getChildWithName("BOOLS");
    for(int i = 0; i < bools.getNumProperties(); i++)
    {
        Identifier id = bools.getPropertyName(i);
        if(id == oscillatorSectionIdentifiers[4])
        {
            oscOnOffToggle.setToggleState(bools.getProperty(id), dontSendNotification);
        }
    }
}