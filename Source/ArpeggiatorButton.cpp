//
//  ArpeggiatorButton.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 29/11/2015.
//
//

#include "ArpeggiatorButton.hpp"

ArpeggiatorButton::ArpeggiatorButton()
{
    setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, 1.f));
    state = true;
}

ArpeggiatorButton::~ArpeggiatorButton()
{
    
}

void ArpeggiatorButton::setState(bool newState)
{
    state = newState;
    if(state)
        setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, 1.f));
    else
        setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, .5));
}

bool ArpeggiatorButton::getState()
{
    return state;
}

void ArpeggiatorButton::flash()
{
    if(state)
    {
        flashAlpha = 1.f;
        setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, 1.f).overlaidWith (Colours::white.withAlpha (flashAlpha)));
        startTimerHz(25);
    }
}

void ArpeggiatorButton::timerCallback()
{
    // Reduce the alpha level of the flash slightly so it fades out
    flashAlpha -= 0.15f;
    if(state)
    {
        if (flashAlpha < 0.05f)
        {
            flashAlpha = 0.f;
            setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, 1.f).overlaidWith (Colours::white.withAlpha (flashAlpha)));
            stopTimer();
        }
        else
        {
            setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, 1.f).overlaidWith (Colours::white.withAlpha (flashAlpha)));
        }
    }
    else
    {
        setColour(0x1000100, Colour::fromFloatRGBA(127, 0, 0, .5));
        stopTimer();
    }
}