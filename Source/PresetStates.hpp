//
//  PresetStates.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 20/12/2015.
//
//

#ifndef PresetStates_hpp
#define PresetStates_hpp

/// Audio presets
/**
  * This class handles the 16 preset ValueTrees and their corresponding XML files contained within the
  * application's resources folder.
 */
#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"

class PresetStates
{
public:
    /** Constructor 
        @param audio_ A reference to the Audio class @see Audio
     */
    PresetStates(Audio& audio_);
    
    /** Destructor */
    ~PresetStates();
    
    /** Fills the indentifier arrays from the default XML file */
    void fillIdentifiers();
    
    /** Fills the default XML with the audio parameter's default values 
        @return Returns true if successful, false if not
     */
    bool fillDefault();
    
    /** Fills the 16 preset ValueTrees with the values contained in their corresponding XML files. Fills them with the default values if the XML file isn't found 
        @return Returns true if successful, false if not
     */
    bool fillPresets();
    
    /** Sets the audio parameters to those contained in the chosen preset ValueTree 
        @param presetNum The preset number to load
     */
    void selectPreset(int presetNum);
    
    /** Overwrites a chosen preset with the current audio parameters 
        @param presetNum The preset number to overwrite
     */
    void overwritePreset(int presetNum);
    
    /** Initialises a chosen preset to the default values 
        @param presetNum The preset number to initialise
     */
    void initPreset(int presetNum);
    
    /** Saves a preset ValueTree to its corresponding XML file 
        @param presetNum The preset number to save
     */
    void savePresetToFile(int presetNum);
    
    /** Returns the current preset as a ValueTree 
        @return The current preset as a ValueTree object
     */
    ValueTree getCurrentPreset();
    
private:
    Audio& audio; /// A reference to the Audio class 
    
    File resourcesFolder; /// The file path to the resources folder where the preset XMLs are saved
    File zeroPresetFile; /// The file path to the zeroed preset XML file
    Array<Identifier> floatIdentifiers; /// Array of Identifiers for the floating-point parameters
    Array<Identifier> intIdentifiers; /// Array of Identifiers for the integer parameters
    Array<Identifier> boolIdentifiers; /// Array of Identifiers for the boolean parameters
    ValueTree defaultPreset; /// ValueTree to store the default preset
    ValueTree presets[16]; /// Array ValueTrees for the 16 presets
    int currentPresetNum; /// The currently active preset number
};
#endif /* PresetStates_hpp */
