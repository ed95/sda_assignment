//
//  Noise.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 14/11/2015.
//
//

#ifndef Noise_hpp
#define Noise_hpp

/** 
 Noise generator
 */

#include <stdio.h>
#include "BaseOscillator.hpp"

//==============================================================================
class NoiseSound : public SynthesiserSound
{
public:
    /** Constructor. */
    NoiseSound() {};
    
    /** Destructor. */
    ~NoiseSound() {};
    
    /** Inherited from SynthesiserSound. Applies to all notes. */
    bool appliesToNote (int midiNoteNumber) override { return true; };
    
    /** Inherited from SynthesiserSound. Applies to all channels. */
    bool appliesToChannel (int midiChannel) override { return true; };
    
private:
    
};
//==============================================================================

class NoiseVoice : public BaseOscillator
{
public:
    /** Constructor. 
        @param audio_ Reference to the Audio class @see Audio
        @param oscNum The number of this oscillator from 0 - 2
    */
    NoiseVoice(Audio& audio_, int oscNum);
    
    /** Destructor. */
    ~NoiseVoice();
    
    /** Returns true if the voice can play a sound. */
    bool canPlaySound (SynthesiserSound* sound) override;
    
    /** Sets the frequency of the STK generator.
        @param frequency The frequency to set the generator
     */
    void setSTKFreq(float frequency) override;
    
    /** Resets the STK generator. */
    void resetSTKGen() override;
    
    /** Renders the waveshape. 
        @return The sample value for this waveshape
     */
    float renderWaveshape() override;
    
private:
    stk::Noise noiseGen; /// STK noise generator
};

#endif /* Noise_hpp */