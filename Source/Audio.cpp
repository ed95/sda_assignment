//
//  Audio.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 25/11/2015.
//
//

#include "Audio.hpp"
#include "SinOscillator.hpp"
#include "SquareOscillator.hpp"
#include "SawOscillator.hpp"
#include "Noise.hpp"
#include "Arpeggiator.hpp"

//==========================================================================================
Audio::Audio() : arpeggiator(*this)
{
    // parameter initialisation
    sampleRate = new FloatParameter(defaultSampleRate, Parameters::pSampleRate, 1.f);
    masterLevel = new FloatParameter(defaultMasterLevel, Parameters::pMasterLevel, 0.02);
    
    oscLevel[0] = new FloatParameter(defaultOscLevel, Parameters::pOscLevel0, 0.01);
    oscLevel[1] = new FloatParameter(defaultOscLevel, Parameters::pOscLevel1, 0.01);
    oscLevel[2] = new FloatParameter(defaultOscLevel, Parameters::pOscLevel2, 0.01);
    
    octaveShift[0] = new IntegerParameter(defaultOctaveShift, -3, 3, Parameters::pOctaveShift0, 1);
    octaveShift[1] = new IntegerParameter(defaultOctaveShift, -3, 3, Parameters::pOctaveShift1, 1);
    octaveShift[2] = new IntegerParameter(defaultOctaveShift, -3, 3, Parameters::pOctaveShift2, 1);
    
    detuneAmount[0] = new IntegerParameter(defaultDetune, -120, 120, Parameters::pDetuneAmount0, 1);
    detuneAmount[1] = new IntegerParameter(defaultDetune, -120, 120, Parameters::pDetuneAmount1, 1);
    detuneAmount[2] = new IntegerParameter(defaultDetune, -120, 120, Parameters::pDetuneAmount2, 1);
    
    oscWaveshape[0] = new IntegerParameter(defaultOscWaveshape, 0, 3, Parameters::pOscWaveshape0, 1);
    oscWaveshape[1] = new IntegerParameter(defaultOscWaveshape, 0, 3, Parameters::pOscWaveshape1, 1);
    oscWaveshape[2] = new IntegerParameter(defaultOscWaveshape, 0, 3, Parameters::pOscWaveshape2, 1);
    
    oscEnabled[0] = new BooleanParameter(defaultOscEnabled, Parameters::pOscEnabled0);
    oscEnabled[1] = new BooleanParameter(defaultOscEnabled, Parameters::pOscEnabled1);
    oscEnabled[2] = new BooleanParameter(defaultOscEnabled, Parameters::pOscEnabled2);
    
    aAttack = new FloatParameter(defaultAAttack, Parameters::pAAttack, 0.01);
    aDecay = new FloatParameter(defaultADecay, Parameters::pADecay, 0.01);
    aSustain = new FloatParameter(defaultASustain, Parameters::pASustain, 0.01);
    aRelease = new FloatParameter(defaultARelease, Parameters::pARelease, 0.01);
    
    fAttack = new FloatParameter(defaultFAttack, Parameters::pFAttack, 0.01);
    fDecay = new FloatParameter(defaultFDecay, Parameters::pFDecay, 0.01);
    fSustain = new FloatParameter(defaultFSustain, Parameters::pFSustain, 0.01);
    fRelease = new FloatParameter(defaultFRelease, Parameters::pFRelease, 0.01);
    filterDepth = new FloatParameter(defaultFilterDepth, Parameters::pFilterDepth, 1.f);
    filterCutoff = new IntegerParameter(defaultFilterCutoff, 20, 20000, Parameters::pFilterCutoff, 0.05);
    filterType = new IntegerParameter(defaultFilterType, 0, 2, Parameters::pFilterType, 1);
    filterQ = new FloatParameter(defaultFilterQ, Parameters::pFilterQ, 0.1);
    
    arpeggiatorBPM = new IntegerParameter(defaultArpeggiatorBPM, 20, 250, Parameters::pArpeggiatorBPM, 0.1);
    arpeggiatorResolution = new IntegerParameter(defaultArpeggiatorResolution, 4, 24, Parameters::pArpeggiatorResolution, 1);
    arpeggiatorSwing = new IntegerParameter(defaultArpeggiatorSwing, -100, 100, Parameters::pArpeggiatorSwing, 0.5);
    arpeggiatorVelocity = new IntegerParameter(defaultArpeggiatorVelocity, 0, 127, Parameters::pArpeggiatorVelocity, 0.5);
    arpeggiatorGate = new FloatParameter(defaultArpeggiatorGate, Parameters::pArpeggiatorGate, 0.5);
    arpeggiatorPattern = new IntegerParameter(defaultArpeggiatorPattern, 1, 3, Parameters::pArpeggiatorPattern, 1);
    
    int counter = 0;
    for(int paramI = Parameters::pBeatActive0; paramI < Parameters::pArpeggiatorState; paramI++)
    {
        Parameters p = static_cast<Parameters>(paramI);
        beatActive[counter] = new BooleanParameter(defaultBeatActive, p);
        counter++;
    }
    
    arpeggiatorState = new BooleanParameter(defaultArpeggiatorState, Parameters::pArpeggiatorState);
    arpeggiatorLatchState = new BooleanParameter(defaultArpeggiatorLatchState, Parameters::pArpeggiatorLatchState);
    arpeggiatorException = new BooleanParameter(defaultArpeggiatorException, Parameters::pArpeggiatorException);
    arpeggiatorLatchException = new BooleanParameter(defaultArpeggiatorLatchException, Parameters::pArpeggiatorLatchException);
    arpeggiatorVelocityState = new BooleanParameter(defaultArpeggiatorVelocityState, Parameters::pArpeggiatorVelocityState);
    clearFilters = new BooleanParameter(defaultClearFilters, Parameters::pClearFilters);
    
    floatParams.add(masterLevel);
    for(int i = 0; i < 3; i++)
        floatParams.add(oscLevel[i]);
    floatParams.add(aAttack);
    floatParams.add(aDecay);
    floatParams.add(aSustain);
    floatParams.add(aRelease);
    floatParams.add(fAttack);
    floatParams.add(fDecay);
    floatParams.add(fSustain);
    floatParams.add(fRelease);
    floatParams.add(filterQ);
    floatParams.add(filterDepth);
    floatParams.add(arpeggiatorGate);
    floatParams.add(sampleRate);
    
    intParamsOffset = floatParams.size();
    for(int i = 0; i < 3; i++)
        intParams.add(octaveShift[i]);
    for(int i = 0; i < 3; i++)
        intParams.add(detuneAmount[i]);
    for(int i = 0; i < 3; i++)
        intParams.add(oscWaveshape[i]);
    intParams.add(filterType);
    intParams.add(arpeggiatorPattern);
    intParams.add(filterCutoff);
    intParams.add(arpeggiatorBPM);
    intParams.add(arpeggiatorResolution);
    intParams.add(arpeggiatorSwing);
    intParams.add(arpeggiatorVelocity);
    
    boolParamsOffset = intParams.size() + intParamsOffset;
    for(int i = 0; i < 3; i++)
        boolParams.add(oscEnabled[i]);
    for(int i = 0; i < 8; i++)
        boolParams.add(beatActive[i]);
    boolParams.add(arpeggiatorState);
    boolParams.add(arpeggiatorLatchState);
    boolParams.add(arpeggiatorVelocityState);
    boolParams.add(arpeggiatorException);
    boolParams.add(arpeggiatorLatchException);
    
    // osc setup
    for(int i = 0; i < NUMOSCS; i++)
    {
        osc[i].clearVoices();
        osc[i].clearSounds();
        
        for(int j = 0; j < NUMVOICES; j++)
        {
            osc[i].addVoice(new SinVoice(*this, i));
        }
        
        osc[i].addSound(new SinSound());
    }
    
    // audio device initialisation
    audioDeviceManager.initialiseWithDefaultDevices (0, 2); // 0 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    audioDeviceManager.setMidiInputEnabled("Keystation Mini 32", true);
    ioDevice = audioDeviceManager.getCurrentAudioDevice();
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    mmCollector.addMessageToQueue(message);
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    updateParamatersFromQueue();
    
    AudioSampleBuffer sampleBuffer = AudioSampleBuffer(outputChannelData, numOutputChannels, numSamples);
    sampleBuffer.clear();
    
    MidiBuffer midiMessages;
    mmCollector.removeNextBlockOfMessages(midiMessages, numSamples);
    
    //only use the arpeggiator if it is toggled on
    if(arpeggiatorState->getValue())
        arpeggiator.fillBuffer(sampleBuffer, midiMessages);
    
    //catch notes left on after arp or latch have been toggled off
    if(arpeggiatorException->getValue() || arpeggiatorLatchException->getValue())
    {
        arpeggiator.handleException(midiMessages);
        arpeggiatorException->setValue(false);
        arpeggiatorLatchException->setValue(false);
    }
    
    for(int i = 0; i < NUMOSCS; i++)
    {
        if(oscEnabled[i]->getValue())
            osc[i].renderNextBlock (sampleBuffer, midiMessages, 0, numSamples);
    }
    
    for(int i = 0; i < numOutputChannels; i++)
        sampleBuffer.applyGain(i, 0, numSamples, masterLevel->getValue());
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sampleRate->setValue(device->getCurrentSampleRate());
    mmCollector.reset(sampleRate->getValue());
    
    for(int i = 0; i < NUMOSCS; i++)
        osc[i].setCurrentPlaybackSampleRate(sampleRate->getValue());
}

void Audio::audioDeviceStopped()
{
    
}

//====================================================================================
void Audio::setWaveshape(int oscNum, int waveshapeIndex)
{
    if(waveshapeIndex >= 0 && waveshapeIndex < 4)
    {
        osc[oscNum].clearVoices();
        osc[oscNum].clearSounds();
        
        if(waveshapeIndex == 0)
        {
            for (int i = 0; i < NUMVOICES; i++)
            {
                osc[oscNum].addVoice(new SinVoice(*this, oscNum));
            }
            osc[oscNum].addSound(new SinSound());
        }
        else if(waveshapeIndex == 1)
        {
            for (int i = 0; i < NUMVOICES; i++)
            {
                osc[oscNum].addVoice(new SquareVoice(*this, oscNum));
            }
            osc[oscNum].addSound(new SquareSound());
        }
        else if(waveshapeIndex == 2)
        {
            for (int i = 0; i < NUMVOICES; i++)
            {
                osc[oscNum].addVoice(new SawVoice(*this, oscNum));
            }
            osc[oscNum].addSound(new SawSound());
        }
        else if(waveshapeIndex == 3)
        {
            for (int i = 0; i < NUMVOICES; i++)
            {
                osc[oscNum].addVoice(new NoiseVoice(*this, oscNum));
            }
            osc[oscNum].addSound(new NoiseSound());
        }
        
        oscWaveshape[oscNum]->setValue(waveshapeIndex);
    }
}


void Audio::updateParamatersFromQueue()
{    
    while(queue.getNumReady())
    {
        ParameterPair param;
        queue.readFromFifo(&param, 1);
        if(param.identifier == Parameters::pReset)
        {
            arpeggiator.reset();
            clearFilters->setValue(true);
            mmCollector.reset(sampleRate->getValue());
            for(int i = 0; i < 3; i++)
                osc[i].allNotesOff(1, false);
        }
        else
        {
            if(param.t == ParameterPair::Float)
            {
                floatParams[param.identifier]->setValue(param.fVal);
            }
            else if(param.t == ParameterPair::Int)
            {
                intParams[param.identifier - intParamsOffset]->setValue(param.iVal);
            }
            else if(param.t == ParameterPair::Bool)
            {
                boolParams[param.identifier - boolParamsOffset]->setValue(param.bVal);
            }
        }
    }
}

void Audio::pushCurrentParametersToQueue()
{
    ParameterPair param;
    
    for(int i = 0; i < floatParams.size() - 1; i++)
    {
        param.identifier = floatParams[i]->getName();
        param.t = ParameterPair::Float;
        param.fVal = floatParams[i]->getValue();
        presetQueue.addToFifo(&param, 1);
    }
    
    for(int i = 0; i < intParams.size(); i++)
    {
        param.identifier = intParams[i]->getName();
        param.t = ParameterPair::Int;
        param.iVal = intParams[i]->getValue();
        presetQueue.addToFifo(&param, 1);
    }
    
    for(int i = 0; i < boolParams.size() - 2; i++)
    {
        param.identifier = boolParams[i]->getName();
        param.t = ParameterPair::Bool;
        param.bVal = boolParams[i]->getValue();
        presetQueue.addToFifo(&param, 1);
    }
}

float Audio::getFloatParameterValue(int param, bool getDefault)
{
    return getDefault ? floatParams[param]->getDefaultValue() : floatParams[param]->getValue();
}

int Audio::getIntParameterValue(int param, bool getDefault)
{
    return getDefault ? intParams[param]->getDefaultValue() : intParams[param]->getValue();
}

bool Audio::getBoolParameterValue(int param, bool getDefault)
{
    return getDefault ? boolParams[param]->getDefaultValue() : boolParams[param]->getValue();
}