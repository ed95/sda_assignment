//
//  Arpeggiator.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 29/11/2015.
//
//

#ifndef Arpeggiator_hpp
#define Arpeggiator_hpp

/**
 Class containing the arpeggiator processes
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "DataQueue.hpp"
#include "AudioParameters.h"

class Audio;

class Arpeggiator : public Timer
{
public:
    /** Constructor.
        @param audio_ A reference to the Audio class @see Audio
     */
    Arpeggiator(Audio& audio_);
    
    /** Destructor. */
    ~Arpeggiator();
    
    /** Fills the MIDI buffer with timestamped MIDI events for arpeggiator note ons and offs. Called in the Audio callback. @see Audio
        @param outputBuffer A reference to the output AudioSampleBuffer to be filled
        @param mBuffer A reference to the MidiBuffer to be filled
     */
    void fillBuffer(AudioSampleBuffer& outputBuffer, MidiBuffer& mBuffer);
    
    /** Called once after the arpeggiator or the arpeggiator latch mode are turned off.
        @param mBuffer A reference to the MidiBuffer
     */
    void handleException(MidiBuffer& mBuffer);
    
    /** Builds a pattern of notes based on the currently held down notes and the type of pattern selected.
        @param patternType The pattern type - ascending (1), descending(2) or random(3)
     */
    void buildPattern(int patternType);

    /** Returns the note number of the next note in the pattern.
        @return The MIDI note number of the next pattern note
     */
    int getNextNote();
    
    /** Resets all the arpeggiator variables. */
    void reset();
    
    /** Calculates the number of samples per beat based on the BPM and swing amount */
    void updateBPM();
    
    /** Updates the note arrays based on which notes are held down or have been released.
        @param mb A reference to the MidiBuffer
     */
    bool updateNoteOns(MidiBuffer& mb);
    
    /** Returns the number of notes currently on.
        @return The number of notes that are currently on
     */
    int getNumNotesOn();
    
    /** Returns true if the buffer spans the next beat.
        @param A reference to the AudioSampleBuffer
        @return Returns true if the buffer spans the next beat, false if not
     */
    bool bufferSpansNextBeat(AudioSampleBuffer& buffer);
    
    /** Returns the number of samples until the next beat.
        @return The number of samples until the next beat
     */
    int getSamplesToNextBeat();
    
    /** Returns the number of samples until the current note should turn off.
        @return The number of samples until the current note should turn off
     */
    int getSamplesToOff();
    
    /**
     Listener class.
     */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener(){}
        
        /** Called when the arpeggiator button should flash.
            @param buttonNum The button number that should flash
         */
        virtual void shouldFlash (int buttonNum) = 0;
    };
    
    /** Sets the listener.
        @param newListener A pointer to the new listener
     */
    void setListener (Listener* newListener);
//======================================================================================
    
    /** Inherited timer function */
    void timerCallback() override;
    
    bool clearNotes; /// If true and latch is on new notes overwrite the current latch notes.
    bool shouldUpdate; /// If true arp pattern should update
    
private:
    Array<int> steps; /// contains note numbers for arp patters
    int counter; /// used to step through the arp patter 
    int lastNote; /// holds the last note of the pattern - makes sure same note isn't played consecutively 
    bool randomMode; /// true if selected pattern is random 
    
    Audio& audio; /// reference to audio object to access audio functions 
    float sampleRate; /// sample rate 
    
    bool noteOn[128]; /// array index = MIDI note, true if on 
    int noteVel[128]; /// holds velocity value for note 
    bool shouldSendNoteOff[128]; /// true if note should be turned off in next MIDI buffer 
    bool latchNotes[128]; /// true if note is on & latch mode is on 
    
    int samplesToOff[128]; /// number of samples until note should turn off 
    int samplesToOn[128]; /// number of smaples until note should turn on 
    bool newNote; /// true if a new note should be grabbed from the arp pattern 
    int note; /// holds the note from the arp pattern 
    
    int sampleCount; /// keeps track of the number of samples from 0 - maxSamples 
    int maxSamples; /// the maximum number of samples based on the num beats & BPM 
    int samplesPerBeat; /// number of samples for each beat 
    int beatNum; /// the current beat number 
    
    bool firstArp; /// true if it's the first arpeggiator since being reset 
    
    int swingSamples; /// sample offset based on swing amount 
    bool newLatch; /// true if latch is toggled on, makes sure notes are copied over 
    float gate; /// % of beat each note should be on for 
    
    Listener* listener; /// listener 
};
#endif /* Arpeggiator_hpp */
