//
//  BaseOscillator.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 01/11/2015.
//
//

#ifndef BaseOscillator_hpp
#define BaseOscillator_hpp

/**
 Base class for oscillators
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "LPF.hpp"
#include "HPF.hpp"
#include "BPF.hpp"

class BaseOscillator : public SynthesiserVoice
{
public:
    /** Constructor.
        @param audio_ A reference to the Audio class @see Audio
        @param oscNum_ This oscillator's number from 0 - 2
     */
    BaseOscillator(Audio& audio_, int oscNum_);
    
    /** Destructor. */
    virtual ~BaseOscillator();
    
    /** Inherited from SynthesiserVoice. */
    virtual bool canPlaySound (SynthesiserSound* sound) override = 0;
    
    /** Inherited from SynthesiserVoice. */
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* sound,
                    int currentPitchWheelPosition) override;
    
    /** Inherited from SynthesiserVoice. */
    void stopNote (float velocity, bool allowTailOff) override;
    
    /** Inherited from SynthesiserVoice. */
    void pitchWheelMoved (int newValue) override;
    
    /** Inherited from SynthesiserVoice. */
    void controllerMoved (int controllerNumber, int newValue) override;
    
    /** Inherited from SynthesiserVoice, renders the next buffer of audio. */
    void renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples) override;
    
    /** Virtual function for oscillators to inherit and render unique waveshapes.
        @return The unique waveshape
     */
    virtual float renderWaveshape() = 0;
    
    /** Virtual function which sets the frequency of the STK generator.
        @param frequency The frequency to set the STK generator
     */
    virtual void setSTKFreq(float frequency) = 0;
    
    /** Virtual function which resets the STK generator. */
    virtual void resetSTKGen() = 0;
    
    /** Called each block to update all the parameters from the Audio class */
    void updateParameters();
    
    /** Eliminate very small or very large numbers. Adapted from the SuperCollider source code.
        @param x The input to be tested
        @return The result of the test. 0 if smaller than 1e-15 or larger than 1e15, unaltered if not
    */
    inline float zapGremlins(float x)
    {
        float absx = std::abs(x);
        return (absx > (float)1e-15 && absx < (float)1e15) ? x : (float)0.;
    }
    
private:
    Audio& audio; /// reference to Audio object
    int oscNum; /// the index of this oscillator from 0 - 2 
    
    float amplitude; /// amplitude of note based on velocity 
    float level; /// level of this oscillator 
    float frequency; /// frequency of note 
    float detuneAmount; /// amount to detune note in cents 
    int octaveShift; /// octave shift amount 
    bool updatePitch; /// true if the pitch should be changed 
    
    stk::ADSR ampEnv; /// amplitude envelope 
    stk::ADSR filterEnv; /// filter envelope 
    LPF lowpassFilter; /// lowpass filter
    HPF highpassFilter; /// highpass filter
    BPF bandpassFilter; /// bandpass filter
    
    float filterDepth; /// depth of filter envelope 
    float filterCutoff; /// cutoff of the filter 
    bool updateCutoff; /// true if cutoff should be updated 
    bool isRising; /// true if the filter envelope is rising 
    int filterType; /// 0 = LPF, 1 = HPF, 2 = BPF
    
    bool resetFilter; /// true if the filters need to be reset
};

#endif /* BaseOscillator_hpp */