/*
  ==============================================================================

    SynthUI.h
    Created: 24 Nov 2015 4:48:05pm
    Author:  Edward Davies

  ==============================================================================
*/

#ifndef SYNTHUI_H_INCLUDED
#define SYNTHUI_H_INCLUDED

/**
 High-level UI containing all the other UI elements
 */

#include "../JuceLibraryCode/JuceHeader.h"
#include "OscillatorSection.hpp"
#include "FilterSection.hpp"
#include "ADSRSection.hpp"
#include "MasterLevelSection.hpp"
#include "ArpeggiatorSection.hpp"
#include "PresetSection.hpp"
#include "TitleSection.hpp"
#include "Audio.hpp"

class SynthUI : public Component
{
public:
    /** Constructor.
        @param audio_ A reference to the Audio class @see Audio
     */
    SynthUI(Audio& audio_);
    
    /** Destructor. */
    ~SynthUI();
    
    /** Overridden from Component. */
    void paint(Graphics&) override;
    
    /** Overridden from Component. */
    void resized() override;
    
private:
    Audio& audio; /// A reference to the Audio class
    PresetSection presetSection;
    OscillatorSection oscillatorSection1, oscillatorSection2, oscillatorSection3;
    FilterSection filterSection;
    ADSRSection ADSRSectionAmp, ADSRSectionFilter;
    MasterLevelSection masterLevelSection;
    ArpeggiatorSection arpeggiatorSection;
    TitleSection titleSection;
    Label oscOnOffLabel, oscOctaveShiftLabel, oscDetuneLabel, oscLevelLabel;
};



#endif  // SYNTHUI_H_INCLUDED
