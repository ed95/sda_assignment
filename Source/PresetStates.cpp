//
//  PresetStates.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 20/12/2015.
//
//

#include "PresetStates.hpp"

PresetStates::PresetStates(Audio& audio_) : audio(audio_)
{
    resourcesFolder = File::getSpecialLocation(File::currentApplicationFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile("resourcesFolder");
    
    if(!resourcesFolder.isDirectory())
        resourcesFolder.createDirectory();
    
    zeroPresetFile = resourcesFolder.getChildFile("zeroPreset.xml");
    if(zeroPresetFile.existsAsFile())
    {
        fillIdentifiers();
        
        if(fillDefault())
        {
            if(!fillPresets())
            {
                DBG("Error filling presets");
            }
        }
        else
        {
            DBG("Error filling default");
        }
    }
    else
    {
        DBG("Zeroed presets XML file does not exist");
    }
}

PresetStates::~PresetStates()
{
    
}

void PresetStates::fillIdentifiers()
{
    XmlDocument doc(zeroPresetFile);
    ScopedPointer<XmlElement> xml(doc.getDocumentElement());
    
    ValueTree tree = ValueTree::fromXml(*xml);
    
    floatIdentifiers.clear();
    intIdentifiers.clear();
    boolIdentifiers.clear();
    
    ValueTree floats = tree.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        floatIdentifiers.add(floats.getPropertyName(i));
    }
    
    ValueTree ints = tree.getChildWithName("INTS");
    for(int i = 0; i < ints.getNumProperties(); i++)
    {
        intIdentifiers.add(ints.getPropertyName(i));
    }
    
    ValueTree bools = tree.getChildWithName("BOOLS");
    for(int i = 0; i < bools.getNumProperties(); i++)
    {
        boolIdentifiers.add(bools.getPropertyName(i));
    }
}

bool PresetStates::fillDefault()
{
    XmlDocument doc(zeroPresetFile);
    ScopedPointer<XmlElement> xml(doc.getDocumentElement());
    
    defaultPreset = ValueTree::fromXml(*xml);
    
    ValueTree floats = defaultPreset.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        floats.setProperty(floatIdentifiers[i], audio.getFloatParameterValue(i, true), nullptr);
    }
    
    ValueTree ints = defaultPreset.getChildWithName("INTS");
    for(int i = 0; i < ints.getNumProperties(); i++)
    {
        ints.setProperty(intIdentifiers[i], audio.getIntParameterValue(i, true), nullptr);
    }
    
    ValueTree bools = defaultPreset.getChildWithName("BOOLS");
    for(int i = 0; i < bools.getNumProperties(); i++)
    {
        bools.setProperty(boolIdentifiers[i], audio.getBoolParameterValue(i, true), nullptr);
    }
    
    return defaultPreset.isValid();
}

bool PresetStates::fillPresets()
{
    for(int i = 0; i < 16; i++)
    {
        String fileNameFull = "preset" + String(i + 1) + ".xml";
        File presetFile = resourcesFolder.getChildFile(fileNameFull);
        if(presetFile.existsAsFile())
        {
            XmlDocument doc(presetFile);
            ScopedPointer<XmlElement> xml(doc.getDocumentElement());
            
            presets[i] = ValueTree::fromXml(*xml);
        }
        else
        {
            presets[i] = defaultPreset.createCopy();
            savePresetToFile(i);
        }
        
        if(!presets[i].isValid())
            return false;
    }
    
    return true;
}

void PresetStates::selectPreset(int presetNum)
{
    // tell audio to reset the audio callback to avoid audio glitches
    ParameterPair reset;
    reset.identifier = Parameters::pReset;
    reset.t = ParameterPair::Bool;
    reset.bVal = true;
    audio.queue.addToFifo(&reset, 1);
    
    // push all the paramters from the selected preset onto the audio queue
    ValueTree floats = presets[presetNum].getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)i;
        param.fVal = floats.getProperty(floatIdentifiers[i]);
        param.t = ParameterPair::Float;
        audio.queue.addToFifo(&param, 1);
    }
    
    ValueTree ints = presets[presetNum].getChildWithName("INTS");
    for(int i = Parameters::pOctaveShift0; i < ints.getNumProperties() + Parameters::pOctaveShift0; i++)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)i;
        param.iVal = ints.getProperty(intIdentifiers[i - Parameters::pOctaveShift0]);
        param.t = ParameterPair::Int;
        audio.queue.addToFifo(&param, 1);
    }
    
    ValueTree bools = presets[presetNum].getChildWithName("BOOLS");
    for(int i = Parameters::pOscEnabled0; i < bools.getNumProperties() + Parameters::pOscEnabled0; i++)
    {
        ParameterPair param;
        
        param.identifier = (Parameters)i;
        param.bVal = bools.getProperty(boolIdentifiers[i - Parameters::pOscEnabled0]);
        param.t = ParameterPair::Bool;
        audio.queue.addToFifo(&param, 1);
    }
    
    currentPresetNum = presetNum;
}

void PresetStates::overwritePreset(int presetNum)
{
    ValueTree floats = presets[presetNum].getChildWithName("FLOATS");
    ValueTree ints = presets[presetNum].getChildWithName("INTS");
    ValueTree bools = presets[presetNum].getChildWithName("BOOLS");
    
    audio.pushCurrentParametersToQueue();
    while(audio.presetQueue.getNumReady())
    {
        ParameterPair param;
        audio.presetQueue.readFromFifo(&param, 1);
        
        if(param.t == ParameterPair::Float)
            floats.setProperty(floatIdentifiers[param.identifier], param.fVal, nullptr);
        else if(param.t == ParameterPair::Int)
            ints.setProperty(intIdentifiers[param.identifier - 16], param.iVal, nullptr);
        else if(param.t == ParameterPair::Bool)
            bools.setProperty(boolIdentifiers[param.identifier - 32], param.bVal, nullptr);
    }
    
    savePresetToFile(presetNum);
}

void PresetStates::initPreset(int presetNum)
{
    presets[presetNum] = defaultPreset.createCopy();
    
    savePresetToFile(presetNum);
    currentPresetNum = presetNum;
}

void PresetStates::savePresetToFile(int presetNum)
{
    String fileNameFull = "preset" + String(presetNum + 1) + ".xml";
    File saveFile = resourcesFolder.getChildFile(fileNameFull);

    if (!saveFile.existsAsFile())
        saveFile.create();
    
    if (presets[presetNum].isValid())
    {
        ScopedPointer<XmlElement> xml (presets[presetNum].createXml());
        xml->writeToFile(saveFile, "");
    }
    else
    {
        DBG("Settings not valid to save");
    }
}

ValueTree PresetStates::getCurrentPreset()
{
    return presets[currentPresetNum].createCopy();
}