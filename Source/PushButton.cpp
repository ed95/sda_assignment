//
//  PushButton.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 25/11/2015.
//
//

#include "PushButton.hpp"

PushButton::PushButton(const String& componentName) : Button(componentName)
{
    File location = location.getSpecialLocation(File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile("images");
    
    File upPath = File(location.getChildFile("redButtonSquareInactive.png"));
    File downPath = File(location.getChildFile("redButtonSquareActive.png"));
    upImage = ImageFileFormat::loadFrom(upPath);
    downImage = ImageFileFormat::loadFrom(downPath);
    setClickingTogglesState(true);
}

PushButton::~PushButton()
{
    
}

void PushButton::paintButton(juce::Graphics &g, bool isMouseOverButton, bool isButtonDown)
{
    if(getToggleState())
    {
        g.drawImageWithin(downImage, 0, 0, getWidth(), getHeight(), RectanglePlacement::centred);;
    }
    else
    {
        g.drawImageWithin(upImage, 0, 0, getWidth(), getHeight(), RectanglePlacement::centred);
    }
}