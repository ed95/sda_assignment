//
//  ADSRSection.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#ifndef ADSRSection_hpp
#define ADSRSection_hpp

/**
 ADSR section UI.
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "PresetSection.hpp"

class ADSRSection : public Component,
public Slider::Listener,
public PresetSection::Listener
{
public:
    /** Constructor.
        @param audio_ A reference to the Audio class @see Audio
        @param appliesTo Sets whether the sliders apply to the filter or amplitude envelope. 0 for amp, 1 for filter
        @param presetSection_ A reference to the PresetSection object so this class can listen to it @see PresetSection
     */
    ADSRSection(Audio& audio_, int appliesTo, PresetSection& presetSection_);
    
    /** Destructor. */
    ~ADSRSection();
    
    /** Overridden from Component. */
    void paint(Graphics&) override;
    
    /** Overridden from Component. */
    void resized() override;
    
    /** Slider listener.
        @param slider The slider that has changed.
     */
    void sliderValueChanged (Slider* slider) override;
    
    /** PresetSection listener.
        @param currentPreset A ValueTree object containing the selected preset so this section can update its UI
     */
    void shouldUpdate(ValueTree currentPreset) override;
    
private:
    Audio& audio; /// A reference to the Audio class
    PresetSection& presetSection; /// A reference to the PresetSection class
    Label attackLabel, decayLabel, sustainLabel, releaseLabel;
    Slider attackSlider, decaySlider, sustainSlider, releaseSlider;
    int envDestination; /// Which envelope this instance applies to
    Array<Identifier> adsrSectionIdentifiers; /// An array of this sections identifiers so it can update from the value tree
};

#endif /* ADSRSection_hpp */
