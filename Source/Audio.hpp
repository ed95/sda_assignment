//
//  Audio.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 25/11/2015.
//
//

#ifndef Audio_hpp
#define Audio_hpp

/**
 Main audio class
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Arpeggiator.hpp"
#include "AudioParameters.h"
#include "DataQueue.hpp"

#define NUMVOICES 8

class Audio : public MidiInputCallback,
public AudioIODeviceCallback
{
public:
    /** Constructor. */
    Audio();
    
    /** Destructor. */
    ~Audio();
    
    /** Returns the audio device manager.
        @return Reference to the AudioDeviceManager object
     */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /** MIDI message callback. Inherited from MidiInputCallback */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /** Audio callback. Inherited from AudioIODeviceCallback. */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /** Set up audio here. Inherited from AudioIODeviceCallback. */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** Release resources, memory etc. Inherited from AudioIODeviceCallback. */
    void audioDeviceStopped() override;
//====================================================================================
    
    /** Updates the audio parameters from the GUI using a lock-free queue */
    void updateParamatersFromQueue();
    
    /** Pushes the current values of all the audio parameters to the preset queue */
    void pushCurrentParametersToQueue();
    
    /** Returns the floating-point audio parameter at the index. Can either return the default or current value. 
        @param param The parameter number to return
        @param getDefault True if function should return the default value, false for current value
        @return The floating-point audio parameter at the index
     */
    float getFloatParameterValue(int param, bool getDefault);
    
    /** Returns the integer audio parameter at the index. Can either return the default or current value.
     @param param The parameter number to return
     @param getDefault True if function should return the default value, false for current value
     @return The integer audio parameter at the index
     */
    int getIntParameterValue(int param, bool getDefault);
    
    /** Returns the boolean audio parameter at the index. Can either return the default or current value.
     @param param The parameter number to return
     @param getDefault True if function should return the default value, false for current value
     @return The boolean audio parameter at the index
     */
    bool getBoolParameterValue(int param, bool getDefault);
//====================================================================================
    
    /** Sets the synthesiser waveshape.
        @param oscNum The oscillator number to change
        @param waveshapeIndex The selected waveshape: 0 = sine, 1 = square, 2 = saw, 3 = noise
     */
    void setWaveshape(int oscNum, int waveshapeIndex);
    
    /** Returns a reference to the arpeggiator object. @see Arpeggiator
        @return A reference to the arpeggiator object
     */
    Arpeggiator& getArpeggiator() { return arpeggiator;};
//====================================================================================

    FloatParameter* sampleRate;
    FloatParameter* masterLevel;
    FloatParameter* oscLevel[NUMOSCS];
    FloatParameter* aAttack;
    FloatParameter* aDecay;
    FloatParameter* aSustain;
    FloatParameter* aRelease;
    FloatParameter* fAttack;
    FloatParameter* fDecay;
    FloatParameter* fSustain;
    FloatParameter* fRelease;
    FloatParameter* filterQ;
    FloatParameter* filterDepth;
    FloatParameter* arpeggiatorGate;
    
    IntegerParameter* octaveShift[NUMOSCS];
    IntegerParameter* detuneAmount[NUMOSCS];
    IntegerParameter* oscWaveshape[NUMOSCS];
    IntegerParameter* filterType;
    IntegerParameter* arpeggiatorPattern;
    IntegerParameter* filterCutoff;
    IntegerParameter* arpeggiatorBPM;
    IntegerParameter* arpeggiatorResolution;
    IntegerParameter* arpeggiatorSwing;
    IntegerParameter* arpeggiatorVelocity;
    
    BooleanParameter* oscEnabled[NUMOSCS];
    BooleanParameter* beatActive[8];
    BooleanParameter* arpeggiatorState;
    BooleanParameter* arpeggiatorLatchState;
    BooleanParameter* arpeggiatorException;
    BooleanParameter* arpeggiatorLatchException;
    BooleanParameter* arpeggiatorVelocityState;
    BooleanParameter* clearFilters;
    
    Array<FloatParameter*> floatParams; /// Array of float parameters
    Array<IntegerParameter*> intParams; /// Array of integer paramters
    Array<BooleanParameter*> boolParams; /// Array of boolean parameters
    
    int intParamsOffset; /// Offset for int paramters
    int boolParamsOffset; /// Offset for bool parameters
//====================================================================================

    DataQueue<ParameterPair> queue; /// The queue for communication between the audio and message thread. Message thread = producer, audio thread = consumer.
    DataQueue<ParameterPair> presetQueue; /// The queue for communication between the audio and message thread. Audio thread = producer, message thread = consumer.
    
private:
    AudioDeviceManager audioDeviceManager; /// The audio device manager
    MidiMessageCollector mmCollector; /// The MIDI message collector
    Synthesiser osc[NUMOSCS]; /// Array of Synthesisers
    AudioIODevice* ioDevice; /// The AudioIODevice
    Arpeggiator arpeggiator; /// The arpeggiator object @see Arpeggiator
    
//=====================================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Audio)
};

#endif /* Audio_hpp */