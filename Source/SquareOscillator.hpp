//
//  SquareOscillator.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 01/11/2015.
//
//

#ifndef SquareOscillator_hpp
#define SquareOscillator_hpp

/**
 Square generator
 */

#include <stdio.h>
#include "BaseOscillator.hpp"

//==============================================================================
class SquareSound : public SynthesiserSound
{
public:
    /* Constructor. */
    SquareSound() {};
    
    /** Destructor. */
    ~SquareSound() {};
    
    /** Inherited from SynthesiserSound. Applies to all notes. */
    bool appliesToNote (int midiNoteNumber) override { return true; };
    
    /** Inherited from SynthesiserSound. Applies to all channels. */
    bool appliesToChannel (int midiChannel) override { return true; };
    
private:
    
};
//==============================================================================

class SquareVoice : public BaseOscillator
{
public:
    /** Constructor.
     @param audio_ Reference to the Audio class @see Audio
     @param oscNum The number of this oscillator from 0 - 2
     */
    SquareVoice(Audio& audio_, int oscNum);
    
    /** Destructor. */
    ~SquareVoice();
    
    /** Returns true if the voice can play a sound. */
    bool canPlaySound (SynthesiserSound* sound) override;
    
    /** Sets the frequency of the STK generator.
     @param frequency The frequency to set the generator
     */
    void setSTKFreq(float frequency) override;
    
    /** Resets the STK generator. */
    void resetSTKGen() override;
    
    /** Renders the waveshape.
     @return The sample value for this waveshape
     */
    float renderWaveshape() override;
    
private:
    stk::BlitSquare squareGen; /// STK bandl-limited square wave generator
};

#endif /* SquareOscillator_hpp */