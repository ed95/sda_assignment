//
//  ArpeggiatorButton.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 29/11/2015.
//
//

#ifndef ArpeggiatorButton_hpp
#define ArpeggiatorButton_hpp

/** 
 Custom button for arpeggiator beats.
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class ArpeggiatorButton : public TextButton,
public Timer
{
public:
    /** Constructor. */
    ArpeggiatorButton();
    
    /** Destructor. */
    ~ArpeggiatorButton();
    
    /** Sets the button as on (full opacity) or off (.5 opacity)
        @param newState The button's state
     */
    void setState(bool newState);
    
    /** Returns the button's on state.
        @return The button's current state. Returns true if on, false if off
    */
    bool getState();
    
    /** Flashes the buton. */
    void flash();
    
    /** Inherited timer callback function. */
    void timerCallback() override;
    
private:
    bool state; /// The button's state
    float flashAlpha; /// The alpha value for flash
};
#endif /* ArpeggiatorButton_hpp */
