//
//  PushButton.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 25/11/2015.
//
//

#ifndef PushButton_hpp
#define PushButton_hpp

/**
 Custom button using a toggle image.
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class PushButton : public Button
{
public:
    /** Constructor. */
    PushButton(const String& componentName = String::empty);
    
    /** Destructor. */
    ~PushButton();
    
    /** Inherited from button. Paints the button */
    void paintButton (Graphics& g,
                      bool isMouseOverButton,
                      bool isButtonDown) override;
private:
    Image upImage; /// The image for the button's "up" state
    Image downImage; /// The image for the button's "down" state
};

#endif /* PushButton_hpp */
