//
//  Noise.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 14/11/2015.
//
//

#include "Noise.hpp"

NoiseVoice::NoiseVoice(Audio& audio_, int oscNum) : BaseOscillator(audio_, oscNum)
{
    
}

NoiseVoice::~NoiseVoice()
{
    
}

bool NoiseVoice::canPlaySound(juce::SynthesiserSound *sound)
{
    return dynamic_cast<NoiseSound*> (sound) != nullptr;
}

void NoiseVoice::setSTKFreq(float frequency)
{
    
}

void NoiseVoice::resetSTKGen()
{

}

float NoiseVoice::renderWaveshape()
{
    return noiseGen.tick();
}