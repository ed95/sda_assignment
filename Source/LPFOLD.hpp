//
//  LPF.hpp
//  pluginTest
//
//  Created by Edward Davies on 16/11/2015.
//
//

#ifndef LPF_hpp
#define LPF_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class LPF
{
public:
    LPF(float sampleRate);
    ~LPF();
    
    void setQ(float q);
    void setCutoff(float frequency);
    float outputFiltered(float inputSample);
    
private:
    stk::BiQuad filter;
    float fSR;
    float qVal;
};

#endif /* LPF_hpp */
