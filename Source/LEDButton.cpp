//
//  LEDButton.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 25/11/2015.
//
//

#include "LEDButton.hpp"

LEDButton::LEDButton(const String& componentName) : Button(componentName)
{
    File location = location.getSpecialLocation(File::currentExecutableFile).getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory().getChildFile("images");
    
    File upPath = File(location.getChildFile("redDotOff.png"));
    File downPath = File(location.getChildFile("redDotOn.png"));
    upImage = ImageFileFormat::loadFrom(upPath);
    downImage = ImageFileFormat::loadFrom(downPath);
    setClickingTogglesState(true);
}

LEDButton::~LEDButton()
{
    
}

void LEDButton::paintButton(juce::Graphics &g, bool isMouseOverButton, bool isButtonDown)
{
    if(getToggleState())
    {
        g.drawImageWithin(downImage, 0, 0, getWidth(), getHeight(), RectanglePlacement::centred);
    }
    else
    {
        g.drawImageWithin(upImage, 0, 0, getWidth(), getHeight(), RectanglePlacement::centred);
    }
}

void LEDButton::flash()
{
    flashAlpha = 1.f;
    setAlpha(flashAlpha);
    startTimerHz(25);
}

void LEDButton::timerCallback()
{
    // Reduce the alpha level of the flash slightly so it fades out
    flashAlpha -= 0.15f;
    if(flashAlpha < 0.05f)
    {
        setAlpha(1.f);
        stopTimer();
    }
    else
    {
        setAlpha(flashAlpha);
    }
}