//
//  SinOscillator.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 01/11/2015.
//
//

#include "SinOscillator.hpp"

SinVoice::SinVoice(Audio& audio_, int oscNum) : BaseOscillator(audio_, oscNum)
{
    
}

SinVoice::~SinVoice()
{
    
}

bool SinVoice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<SinSound*> (sound) != nullptr;
};

void SinVoice::setSTKFreq(float frequency)
{
    sinGen.setFrequency(frequency);
}

void SinVoice::resetSTKGen()
{
    sinGen.reset();
}

float SinVoice::renderWaveshape()
{
    return sinGen.tick();
}