//
//  PresetSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 20/12/2015.
//
//

#include "PresetSection.hpp"

PresetSection::PresetSection(Audio& audio_) : selectLabel("", "SELECT"), writeLabel("", "WRITE"), initLabel("", "INIT"), presetLabel("", "PRESETS"), presets(audio_)
{
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            addAndMakeVisible(&presetToggles[i][j]);
            presetToggles[i][j].addListener(this);
            presetToggles[i][j].setRadioGroupId(1);
            
            if(i == 0)
            {
                numberLabels[j].setText(String(j + 1), dontSendNotification);
                numberLabels[j].attachToComponent(&presetToggles[i][j], false);
                numberLabels[j].setJustificationType(Justification::centred);
            }
            
            if(j == 0)
            {
                letterLabels[i].setText(i == 0 ? "A" : "B", dontSendNotification);
                letterLabels[i].attachToComponent(&presetToggles[i][j], true);
            }
        }
    }
    
    addAndMakeVisible(&selectButton);
    selectButton.addListener(this);
    selectButton.setRadioGroupId(2);
    selectButton.setToggleState(true, dontSendNotification);
    selectMode = true;
    
    addAndMakeVisible(&writeButton);
    writeButton.addListener(this);
    writeButton.setRadioGroupId(2);
    
    addAndMakeVisible(&initButton);
    initButton.addListener(this);
    initButton.setRadioGroupId(2);
    
    addAndMakeVisible(&selectLabel);
    addAndMakeVisible(&writeLabel);
    addAndMakeVisible(&initLabel);
    addAndMakeVisible(&presetLabel);
    
    presetLabel.setJustificationType(Justification::centred);
    
    previousButton = -1;
}

PresetSection::~PresetSection()
{
    
}

void PresetSection::paint(Graphics& g)
{
    g.drawVerticalLine(0, 0, getHeight());
}

void PresetSection::resized()
{
    int componentHeight = (getHeight() - 45) / 3;
    int componentWidth = (getWidth() - 80) / 12;
    
    presetLabel.setBounds(getWidth() / 3, 0, getWidth() / 10, componentHeight);
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            presetToggles[i][j].setBounds(40 + (j * (componentWidth + 5)), presetLabel.getBottom() + 20 + (i * (componentHeight + 5)), componentWidth, componentHeight);
        }
    }
    
    int otherHeight = (getHeight() - 30) / 3;
    selectButton.setBounds(presetToggles[0][7].getRight() + 10, 10, componentWidth, otherHeight);
    writeButton.setBounds(presetToggles[0][7].getRight() + 10, selectButton.getBottom() + 5, componentWidth, otherHeight);
    initButton.setBounds(presetToggles[0][7].getRight() + 10, writeButton.getBottom() + 5, componentWidth, otherHeight);
    
    selectLabel.setBounds(selectButton.getRight(), selectButton.getY(), selectButton.getWidth() * 3, selectButton.getHeight());
    writeLabel.setBounds(writeButton.getRight(), writeButton.getY(), writeButton.getWidth() * 3, writeButton.getHeight());
    initLabel.setBounds(initButton.getRight(), initButton.getY(), initButton.getWidth() * 3, initButton.getHeight());
}

void PresetSection::buttonClicked(Button* button)
{
    if(button == &selectButton && selectButton.getToggleState())
    {
        selectMode = true;
        writeMode = false;
        initMode = false;
    }
    else if(button == &writeButton && writeButton.getToggleState())
    {
        selectMode = false;
        writeMode = true;
        initMode = false;
    }
    else if(button  == &initButton && initButton.getToggleState())
    {
        selectMode = false;
        writeMode = false;
        initMode = true;
    }
    else
    {
        int selected;
        for(int i = 0; i < 2; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                if(button == &presetToggles[i][j] && presetToggles[i][j].getToggleState())
                {
                    if(i == 0)
                        selected = j;
                    else
                        selected = 8 + j;
                    
                    if(selectMode)
                    {
                        presets.selectPreset(selected);
                        previousButton = selected;
                        
                        presetSectionListeners.call(&PresetSection::Listener::shouldUpdate, presets.getCurrentPreset());
                    }
                    else if(writeMode)
                    {
                        presets.overwritePreset(selected);
                        presetToggles[i][j].setToggleState(false, dontSendNotification);
                        presetToggles[i][j].flash();
                        if(previousButton != -1)
                        {
                            if(previousButton < 8)
                                presetToggles[0][previousButton].setToggleState(true, dontSendNotification);
                            else
                                presetToggles[1][previousButton - 8].setToggleState(true, dontSendNotification);
                        }
                    }
                    else if(initMode)
                    {
                        presets.initPreset(selected);
                        presetToggles[i][j].setToggleState(false, dontSendNotification);
                        presetToggles[i][j].flash();
                        
                        if(selected == previousButton)
                        {
                            presets.selectPreset(selected);
                            presetSectionListeners.call(&PresetSection::Listener::shouldUpdate, presets.getCurrentPreset());
                            presetToggles[i][j].setToggleState(true, dontSendNotification);
                        }
                        else if(previousButton != -1)
                        {
                            if(previousButton < 8)
                                presetToggles[0][previousButton].setToggleState(true, dontSendNotification);
                            else
                                presetToggles[1][previousButton - 8].setToggleState(true, dontSendNotification);
                        }
                    }
                    
                }
            }
        }
    }
}

void PresetSection::setListener(Listener* newListener)
{
    presetSectionListeners.add(newListener);
}