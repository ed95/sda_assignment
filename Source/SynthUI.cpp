/*
  ==============================================================================

    SynthUI.cpp
    Created: 24 Nov 2015 4:48:05pm
    Author:  Edward Davies

  ==============================================================================
*/

#include "SynthUI.h"

SynthUI::SynthUI(Audio& audio_) : audio (audio_), presetSection(audio_), oscillatorSection1(audio_, 0, presetSection), oscillatorSection2(audio_, 1, presetSection), oscillatorSection3(audio_, 2, presetSection), filterSection(audio_, presetSection), ADSRSectionAmp(audio_, 0, presetSection), ADSRSectionFilter(audio_, 1, presetSection), masterLevelSection(audio_, presetSection), arpeggiatorSection(audio_, presetSection), oscOnOffLabel("", "ON"), oscOctaveShiftLabel("", "OCT SHIFT"), oscDetuneLabel("", "DETUNE"), oscLevelLabel("", "LEVEL")
{
    addAndMakeVisible(&oscillatorSection1);
    addAndMakeVisible(&oscillatorSection2);
    addAndMakeVisible(&oscillatorSection3);
    addAndMakeVisible(&filterSection);
    addAndMakeVisible(&ADSRSectionAmp);
    addAndMakeVisible(&ADSRSectionFilter);
    addAndMakeVisible(&masterLevelSection);
    addAndMakeVisible(&arpeggiatorSection);
    addAndMakeVisible(&presetSection);
    addAndMakeVisible(&titleSection);
    
    addAndMakeVisible(&oscOnOffLabel);
    addAndMakeVisible(&oscOctaveShiftLabel);
    addAndMakeVisible(&oscDetuneLabel);
    addAndMakeVisible(&oscLevelLabel);
    oscOnOffLabel.setJustificationType(Justification::centred);
    oscOctaveShiftLabel.setJustificationType(Justification::centred);
    oscDetuneLabel.setJustificationType(Justification::centred);
    oscLevelLabel.setJustificationType(Justification::centred);
}

SynthUI::~SynthUI()
{
    
}

void SynthUI::paint(juce::Graphics &g)
{
    g.drawHorizontalLine(oscillatorSection3.getBottom() + 20, 0, oscillatorSection3.getWidth());
    g.drawHorizontalLine(oscillatorSection1.getY() - 30, 0, oscillatorSection1.getWidth());
}

void SynthUI::resized()
{
    int headerHeight = getHeight() / 6;
    int remainingHeight = 5 * (getHeight() / 6);
    presetSection.setBounds(getWidth() / 2, 0, getWidth() / 2, headerHeight);
    titleSection.setBounds(0, 0, getWidth() / 2, headerHeight);
    
    int oscSectionHeight = (((remainingHeight / 2) - 110) / 3);
    oscillatorSection1.setBounds(0, headerHeight + 30, getWidth() / 2, oscSectionHeight);
    oscOnOffLabel.setBounds(oscillatorSection1.getComponentX(0), headerHeight + 10, oscillatorSection1.getComponentWidth(0), 20);
    oscOctaveShiftLabel.setBounds(oscillatorSection1.getComponentX(5), oscOnOffLabel.getY(), oscillatorSection1.getComponentWidth(5), 20);
    oscDetuneLabel.setBounds(oscillatorSection1.getComponentX(6), oscOnOffLabel.getY(), oscillatorSection1.getComponentWidth(6), 20);
    oscLevelLabel.setBounds(oscillatorSection1.getComponentX(7), oscOnOffLabel.getY(), oscillatorSection1.getComponentWidth(7), 20);
    
    oscillatorSection2.setBounds(0, oscillatorSection1.getBottom() + 40, getWidth() / 2, oscSectionHeight);
    oscillatorSection3.setBounds(0, oscillatorSection2.getBottom() + 40, getWidth() / 2, oscSectionHeight);
    arpeggiatorSection.setBounds(0, oscillatorSection3.getBottom() + 40, getWidth() / 2, remainingHeight / 2);
    filterSection.setBounds(oscillatorSection1.getRight(), headerHeight, getWidth() / 4, arpeggiatorSection.getHeight());
    ADSRSectionFilter.setBounds(oscillatorSection1.getRight(), filterSection.getBottom(), getWidth() / 4, arpeggiatorSection.getHeight());
    masterLevelSection.setBounds(filterSection.getRight(), headerHeight, getWidth() / 4, arpeggiatorSection.getHeight());
    ADSRSectionAmp.setBounds(ADSRSectionFilter.getRight(), masterLevelSection.getBottom(), getWidth() / 4, arpeggiatorSection.getHeight());
}