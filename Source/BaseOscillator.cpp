//
//  BaseOscillator.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 01/11/2015.
//
//

#include "BaseOscillator.hpp"

BaseOscillator::BaseOscillator(Audio& audio_, int oscNum_) : audio(audio_), oscNum(oscNum_), lowpassFilter(audio_), highpassFilter(audio_), bandpassFilter(audio_)
{
    amplitude = 0.f;
    frequency = 440.f;
    
    updatePitch = false;
    updateCutoff = false;
    resetFilter = false;
}

BaseOscillator::~BaseOscillator()
{
    
}

void BaseOscillator::startNote (int midiNoteNumber, float velocity, SynthesiserSound* sound,
                          int currentPitchWheelPosition)
{
    frequency = MidiMessage::getMidiNoteInHertz(midiNoteNumber);
    updatePitch = true;
    amplitude = velocity;
    
    if(ampEnv.getState() != stk::ADSR::IDLE) //make sure filter env gets retriggered
    {
        ampEnv.setReleaseTime(0.00001);
        ampEnv.keyOff();
    }
    ampEnv.keyOn();
    
    if(filterEnv.getState() != stk::ADSR::IDLE) //make sure filter env gets retriggered
    {
        filterEnv.setReleaseTime(0.001);
        filterEnv.keyOff();
    }
    filterEnv.keyOn();
}

void BaseOscillator::stopNote (float velocity, bool allowTailOff)
{
    ampEnv.keyOff();
    filterEnv.keyOff();
    if(ampEnv.getState() != stk::ADSR::RELEASE)// catch errors
    {
        clearCurrentNote();
        resetSTKGen();
    }
}

void BaseOscillator::pitchWheelMoved (int newValue)
{
    
}

void BaseOscillator::controllerMoved (int controllerNumber, int newValue)
{
    
}

void BaseOscillator::renderNextBlock(AudioSampleBuffer &outputBuffer, int startSample, int numSamples)
{
    updateParameters();
    if(resetFilter)
    {
        clearCurrentNote();
        resetSTKGen();
        
        switch (filterType) {
            case 0:
                lowpassFilter.clearFilter();
                lowpassFilter.setCutoff(20000);
                break;
            case 1:
                highpassFilter.clearFilter();
                highpassFilter.setCutoff(20000);
                break;
            case 2:
                bandpassFilter.clearFilter();
                bandpassFilter.setCutoff(20000);
                break;
            default:
                break;
        }
        
        audio.clearFilters->setValue(false);
    }
    
    if(updatePitch)
    {
        float shift = pow(2, abs(octaveShift)) * pow(2, detuneAmount);
        if(octaveShift > 0)
            setSTKFreq(frequency * shift);
        else if(octaveShift < 0)
            setSTKFreq(frequency / shift);
        else
            setSTKFreq(frequency * pow(2, detuneAmount));
        
        updatePitch = false;
    }
    
    while (--numSamples >= 0)
    {
        float envOut = ampEnv.tick();
        float output = renderWaveshape() * amplitude * level * envOut;
        
        if(updateCutoff)
        {
            float cutoff;
            if(isRising)
            {
                cutoff = filterCutoff + (filterEnv.tick() * (filterDepth * (20000 - filterCutoff)));
            }
            else
            {
                cutoff = filterCutoff - (filterEnv.tick() * (fabs(filterDepth) * (filterCutoff - 20)));
            }
            
            if(filterType == 0)
                lowpassFilter.setCutoff(cutoff);
            else if(filterType == 1)
                highpassFilter.setCutoff(cutoff);
            else if(filterType == 2)
                bandpassFilter.setCutoff(cutoff);
        }
        
        if(filterType == 0)
            output = lowpassFilter.outputFiltered(output);
        else if(filterType == 1)
            output = highpassFilter.outputFiltered(output);
        else if(filterType == 2)
            output = bandpassFilter.outputFiltered(output);
        
        const float currentSample = zapGremlins(output);
        for (int i = outputBuffer.getNumChannels(); --i >= 0;)
        {
            outputBuffer.addSample (i, startSample, currentSample);
        }
        
        ++startSample;
        
        if(envOut == 0) // env has finished so clear note and reset gen
        {
            clearCurrentNote();
            resetSTKGen();
            break;
        }
    }
}

void BaseOscillator::updateParameters()
{
    //ENVELOPES
    level = audio.oscLevel[oscNum]->getValue();
    ampEnv.setAllTimes(audio.aAttack->getValue(), audio.aDecay->getValue(), audio.aSustain->getValue(), audio.aRelease->getValue());
    
    filterType = audio.filterType->getValue();
    filterCutoff = audio.filterCutoff->getValue();
    filterDepth = audio.filterDepth->getValue();
    if(filterDepth == 0)
    {
        updateCutoff = false;
        if(filterType == 0)
            lowpassFilter.setCutoff(filterCutoff);
        else if(filterType == 1)
            highpassFilter.setCutoff(filterCutoff);
        else if(filterType == 2)
            bandpassFilter.setCutoff(filterCutoff);
    }
    else
    {
        isRising = (filterDepth > 0) ? true : false;
        updateCutoff = true;
    }
    
    filterEnv.setAllTimes(audio.fAttack->getValue(), audio.fDecay->getValue(), audio.fSustain->getValue(), audio.fRelease->getValue());
    
    //PITCH
    int oldOctShift = octaveShift;
    octaveShift = audio.octaveShift[oscNum]->getValue();
    
    int oldDetuneAmount = detuneAmount;
    detuneAmount = audio.detuneAmount[oscNum]->getValue() / 1200.f;
    
    if(oldOctShift != octaveShift || oldDetuneAmount != detuneAmount)
        updatePitch = true;
    
    resetFilter = audio.clearFilters->getValue();
}