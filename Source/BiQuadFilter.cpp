//
//  BiQuadFilter.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#include "BiQuadFilter.hpp"

BiQuadFilter::BiQuadFilter(Audio& audio_) : audio(audio_)
{
    filter.clear();
    qVal = audio.filterQ->getDefaultValue();
}

BiQuadFilter::~BiQuadFilter()
{
    
}

float BiQuadFilter::getQ()
{
    qVal = audio.filterQ->getValue();
    return qVal;
}

float BiQuadFilter::getSampleRate()
{
    float sampleRate = audio.sampleRate->getValue();
    return sampleRate;
}

void BiQuadFilter::setCoeffs(float b0, float b1, float b2, float a1, float a2)
{
    filter.setB0(b0);
    filter.setB1(b1);
    filter.setB2(b2);
    
    filter.setA1(a1);
    filter.setA2(a2);
}

float BiQuadFilter::outputFiltered(float inputSample)
{
    return filter.tick(inputSample);
}

void BiQuadFilter::clearFilter()
{
    filter.clear();
}