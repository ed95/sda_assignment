//
//  HPF.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#ifndef HPF_hpp
#define HPF_hpp

/** 
 Highpass filter
 */

#include <stdio.h>
#include "BiQuadFilter.hpp"

class HPF : public BiQuadFilter
{
public:
    /** Constructor.
     @param audio_ A reference to the Audio class @see Audio
     */
    HPF(Audio& audio_);
    
    /** Destructor. */
    ~HPF();
    
    /* Inherited from parent class. Sets the filter cutoff.
     @param frequency The desired cutoff frequency
     */
    void setCutoff(float frequency) override;
    
private:
};

#endif /* HPF_hpp */
