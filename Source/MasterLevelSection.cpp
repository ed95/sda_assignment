//
//  MasterLevelSection.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#include "MasterLevelSection.hpp"
#define NUMOSCS 3

MasterLevelSection::MasterLevelSection(Audio& audio_, PresetSection& presetSection_) : audio (audio_), presetSection(presetSection_), masterLevelLabel("", "MASTER LEVEL")
{
    addAndMakeVisible(&masterLevelSlider);
    masterLevelSlider.setSliderStyle(Slider::Rotary);
    masterLevelSlider.addListener(this);
    masterLevelSlider.setRange(0.f, NUMOSCS, 0.01);
    masterLevelSlider.setValue(audio.masterLevel->getDefaultValue());
    masterLevelLabel.setFont(Font(11.f));
    masterLevelLabel.attachToComponent(&masterLevelSlider, false);
    
    presetSection.setListener(this);
    
    masterLevelIdentifier = Identifier("masterLevel");
}

MasterLevelSection::~MasterLevelSection()
{
    
}

void MasterLevelSection::paint(juce::Graphics &g)
{
    g.drawVerticalLine(0, 0, getHeight());
    g.drawHorizontalLine(0, 0, getWidth());
}

void MasterLevelSection::resized()
{
    masterLevelSlider.setBounds(20, 20, getWidth() - 40, getHeight() - 40);
    
    repaint();
}

void MasterLevelSection::sliderValueChanged(juce::Slider *slider)
{
    if(slider == &masterLevelSlider)
    {
        ParameterPair param;
        
        param.identifier = Parameters::pMasterLevel;
        param.fVal = masterLevelSlider.getValue();
        param.t = ParameterPair::Float;
        audio.queue.addToFifo(&param, 1);
    }
}

void MasterLevelSection::shouldUpdate(ValueTree currentPreset)
{
    ValueTree floats = currentPreset.getChildWithName("FLOATS");
    for(int i = 0; i < floats.getNumProperties(); i++)
    {
        Identifier id = floats.getPropertyName(i);
        if(id == masterLevelIdentifier)
        {
            masterLevelSlider.setValue(floats.getProperty(id));
        }
    }
}