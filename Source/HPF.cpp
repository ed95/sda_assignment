//
//  HPF.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#include "HPF.hpp"

HPF::HPF(Audio& audio_) : BiQuadFilter(audio_)
{
    
}

HPF::~HPF()
{
    
}

void HPF::setCutoff(float frequency)
{
    float qVal = getQ();
    float kVal = tanf(M_PI * frequency / getSampleRate());
    float norm = 1.f / (1.f + kVal / qVal + kVal * kVal);
    
    setCoeffs(norm, -2.f * norm, norm, 2.f * (kVal * kVal - 1.f) * norm, (1.f - kVal / qVal + kVal * kVal) * norm);
}