//
//  BPF.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#include "BPF.hpp"

BPF::BPF(Audio& audio_) : BiQuadFilter(audio_)
{
    
}

BPF::~BPF()
{
    
}

void BPF::setCutoff(float frequency)
{
    float qVal = getQ();
    float kVal = tanf(M_PI * frequency / getSampleRate());
    float norm = 1.f / (1.f + kVal / qVal + kVal * kVal);

    float b0 = kVal / qVal * norm;
    setCoeffs(b0, 0, -b0, 2.f * (kVal * kVal - 1.f) * norm, (1.f - kVal / qVal + kVal * kVal) * norm);
}