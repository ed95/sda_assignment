//
//  LPF.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#include "LPF.hpp"

LPF::LPF(Audio& audio_) : BiQuadFilter(audio_)
{
    
}

LPF::~LPF()
{
    
}

void LPF::setCutoff(float frequency)
{
    float qVal = getQ();
    float kVal = tanf(M_PI * frequency / getSampleRate());
    float norm = 1.f / (1.f + kVal / getQ() + kVal * kVal);
    
    float b0 = kVal * kVal * norm;
    
    setCoeffs(b0, 2 * b0, b0, 2 * (kVal * kVal - 1) * norm, (1 - kVal / qVal + kVal * kVal) * norm);
}