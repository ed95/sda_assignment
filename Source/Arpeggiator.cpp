//
//  Arpeggiator.cpp
//  SDA_Assignment
//
//  Created by Edward Davies on 29/11/2015.
//
//

#include "Arpeggiator.hpp"
#include "Audio.hpp"

Arpeggiator::Arpeggiator(Audio& audio_) : audio(audio_)
{
    counter = 0;
    lastNote = -1;
    randomMode = false;
    newLatch = true;
    
    reset();
}

Arpeggiator::~Arpeggiator()
{
    
}

void Arpeggiator::fillBuffer(AudioSampleBuffer& outputBuffer, MidiBuffer& mBuffer)
{
    updateBPM();
    shouldUpdate = updateNoteOns(mBuffer) || shouldUpdate; //notes or pattern have changed
    
    mBuffer.clear(); //get rid of the original MIDI messages - we'll store them in our own arrays and control when note on/offs are sent
    
    int numNotesOn = getNumNotesOn();
    if(firstArp && numNotesOn != 0) //restart the arpeggiators beat tracking when new chord is held down
    {
        sampleCount = 0;
        firstArp = false;
    }
    
    bool notesReleased = numNotesOn == 0 && shouldUpdate; //catches note offs
    int bufferSize = outputBuffer.getNumSamples();
    
    // only do stuff if notes are held down or have recently been released
    if(numNotesOn != 0 || notesReleased)
    {
        // for all MIDI notes
        for(int i = 0; i < 128; i++)
        {
            // if the note needs to be turned off check if the number of samples until off falls within the current buffer
            if(shouldSendNoteOff[i])
            {
                // if it does or if all notes have been released, turn it off
                if(bufferSize >= samplesToOff[i] || notesReleased)
                {
                    MidiMessage noteOffMessage = MidiMessage::noteOff(1, i);
                    mBuffer.addEvent(noteOffMessage, jmax(0, samplesToOff[i]));
                    
                    shouldSendNoteOff[i] = false;
                }
                else // if it doesn't fall within this buffer, subtract the size of the buffer
                {
                    samplesToOff[i] -= bufferSize;
                }
            }
        }
        
        bool isButtonOn = audio.beatActive[beatNum]->getValue();
        
        if(isButtonOn) // only turn notes on if the button is toggled on
        {
            if(shouldUpdate) //if the pattern has changed, update it here
            {
                int pattern = audio.arpeggiatorPattern->getValue();
                
                buildPattern(pattern);
                shouldUpdate = false;
                newNote = true;
            }
            
            if(newNote)
            {
                note = getNextNote();
                samplesToOn[note] = getSamplesToNextBeat(); // turn on at the next beat
                if(beatNum % 2 != 0 && swingSamples != 0) // if we need to add swing samples, do it here
                {
                    samplesToOn[note] += swingSamples;
                }
            }
            
            if(noteOn[note] || latchNotes[note]) // if the note is held down or was held down on latch
            {
                if(bufferSize >= samplesToOn[note]) // if the on position falls within this buffer
                {
                    int velocity;
                    
                    
                    bool arpVelocity = audio.arpeggiatorVelocityState->getValue();
                    if(arpVelocity)
                        velocity = audio.arpeggiatorVelocity->getValue(); // use set velocity
    
                    if(!arpVelocity)
                        velocity = noteVel[note]; // use MIDI note event velocity
                    
                    // add note on message to MIDI buffer and calculate the number of samples until it should turn off
                    MidiMessage noteOnMsg = MidiMessage::noteOn(1, note, (uint8) velocity);
                    mBuffer.addEvent(noteOnMsg, jmax(0, samplesToOn[note]));
                    shouldSendNoteOff[note] = true;
                    samplesToOff[note] = gate * samplesPerBeat;
                
                    newNote = true; // get a new note next time
                    
                    if(listener != nullptr)
                        listener->shouldFlash(beatNum);
                    
                    beatNum++;
                    if(beatNum > 7)
                        beatNum = 0;
                }
                else
                {
                    samplesToOn[note] -= bufferSize; // not turning note on this buffer so subtract buffer size
                    newNote = false; // don't need a new note yet
                }
            }
        }
        else
        {
            //still increment the beat number if button is off and grab a new note next time
            if(bufferSpansNextBeat(outputBuffer))
            {
//                if(listener != nullptr)
//                    listener->shouldFlash(beatNum);
                
                beatNum++;
                if(beatNum > 7)
                    beatNum = 0;
            }
            newNote = true;
        }
    }
    
    // if all notes have been released let the arp know it should restart beat tracking on next chord
    if(numNotesOn == 0)
    {
        firstArp = true;
        beatNum = 0;
    }
    
    // keep track of where we are in the pattern
    sampleCount += outputBuffer.getNumSamples();
    if(sampleCount > maxSamples)
        sampleCount -= maxSamples;
}

void Arpeggiator::handleException(MidiBuffer& mBuffer)
{
    for(int i = 0; i < 128; i++)
    {
        if(shouldSendNoteOff[i] || noteOn[i] || latchNotes[i])
        {
            MidiMessage noteOffMessage = MidiMessage::noteOff(1, i);
            mBuffer.addEvent(noteOffMessage, 0);
            noteOn[i] = false;
            shouldSendNoteOff[i] = false;
            latchNotes[i] = false;
        }
    }
}

void Arpeggiator::buildPattern(int patternType)
{
    bool latchState = audio.arpeggiatorLatchState->getValue();
    
    if(latchState)
    {
        //if latch is toggled on whilst notes are held down make sure the pattern is copied over
        if(latchState == newLatch)
        {
            for(int i = 0; i < 128; i++)
            {
                if(noteOn[i])
                    latchNotes[i] = true;
            }
            newLatch = false;
            clearNotes = true;
        }
        
        int numOn = 0;
        for(int i = 0; i < 128; i++)
        {
            if(latchNotes[i])
                numOn++;
        }
        
        if(numOn > 0)
            steps.resize(numOn);
        
        steps.clear();
        
        if(patternType == 1) //ascending
        {
            for(int i = 0; i < 128; i++)
            {
                if(latchNotes[i])
                {
                    steps.add(i);
                }
            }
            randomMode = false;
        }
        else if(patternType == 2) //descending
        {
            for(int i = 127; i >= 0; i--)
            {
                if(latchNotes[i])
                {
                    steps.add(i);
                }
            }
            randomMode = false;
        }
        else if(patternType == 3) //random
        {
            for(int i = 0; i < 128; i++)
            {
                if(latchNotes[i])
                {
                    steps.add(i);
                }
            }
            randomMode = true;
        }
        
        counter = 0;
    }
    else
    {
        int numOn = 0;
        for(int i = 0; i < 128; i++)
        {
            if(noteOn[i])
                numOn++;
        }
        
        if(numOn > 0)
            steps.resize(numOn);
        
        steps.clear();
        
        if(patternType == 1) //ascending
        {
            for(int i = 0; i < 128; i++)
            {
                if(noteOn[i])
                {
                    steps.add(i);
                }
            }
            randomMode = false;
        }
        else if(patternType == 2) //descending
        {
            for(int i = 127; i >= 0; i--)
            {
                if(noteOn[i])
                {
                    steps.add(i);
                }
            }
            randomMode = false;
        }
        else if(patternType == 3) //random
        {
            for(int i = 0; i < 128; i++)
            {
                if(noteOn[i])
                {
                    steps.add(i);
                }
            }
            randomMode = true;
        }
        
        counter = 0;
        newLatch = true; // make sure latch notes are remembered
    }
}

int Arpeggiator::getNextNote()
{
    int note;
    
    if(randomMode && steps.size() > 1)
    {
        int rNum = rand() % steps.size();
        while(steps[rNum] == lastNote)
        {
            rNum = rand() % steps.size();
        }
        note = steps[rNum];
    }
    else
    {
        if(steps.size() > 1 && steps[counter] == lastNote)
        {
            counter++;
            if(counter >= steps.size())
                counter = 0;
        }
        
        note = steps[counter];
        counter++;
        if(counter >= steps.size())
            counter = 0;
    }
    
    lastNote = note;
    return note;
}

void Arpeggiator::reset()
{
    for(int i = 0; i < 128; i++)
    {
        noteOn[i] = false;
        noteVel[i] = 0;
        shouldSendNoteOff[i] = false;
        latchNotes[i] = false;
        samplesToOff[i] = 0;
        samplesToOn[i] = 0;
    }
    sampleCount = 0;
    shouldUpdate = false;
    firstArp = true;
    beatNum = 0;
    counter = 0;
    newNote = true;
    note = 0;
    clearNotes = true;
}

void Arpeggiator::updateBPM()
{
    float beatsPerSec;
    float secPerBeat;
    int samplesPerCrotchet;
    
    sampleRate = audio.sampleRate->getValue();
    beatsPerSec =  audio.arpeggiatorBPM->getValue() / 60.f;
    
    secPerBeat = 1.f / beatsPerSec;
    samplesPerCrotchet = secPerBeat * sampleRate;
    
    int resolution = audio.arpeggiatorResolution->getValue();
    int swingAmount = audio.arpeggiatorSwing->getValue();
    gate = audio.arpeggiatorGate->getValue();
    
    samplesPerBeat = samplesPerCrotchet / (resolution / 4.f);
    swingSamples = ((samplesPerCrotchet / (resolution / 1.5))) * (swingAmount / 100.f);
    
    maxSamples = 8 * samplesPerBeat;
}

bool Arpeggiator::updateNoteOns(MidiBuffer &mb)
{
    bool update = false;
    
    // Iterate over the midi buffer
    MidiBuffer::Iterator mIterator(mb);
    MidiMessage mm;
    int eventSamplePos = 0;
    bool event = mIterator.getNextEvent(mm, eventSamplePos);
    while(event)
    {
        bool latchState = audio.arpeggiatorLatchState->getValue();
        
        if(mm.isNoteOn())
        {
            if(latchState)
            {
                if(clearNotes)
                {
                    for(int i = 0; i < 128; i++)
                    {
                        if(latchNotes[i] || noteOn[i])
                        {
                            latchNotes[i] = false;
                            noteOn[i] = false;
                        }
                    }
                    
                    clearNotes = false;
                    startTimer(250); //catch notes that come later
                }
                
                latchNotes[mm.getNoteNumber()] = true;
            }
            else
            {
                noteOn[mm.getNoteNumber()] = true;
            }
            update = true;
            noteVel[mm.getNoteNumber()] = mm.getVelocity();
        }
        else if(mm.isNoteOff() && !latchState) // don't turn off if latch mode is on
        {
            noteOn[mm.getNoteNumber()] = false;
            shouldSendNoteOff[mm.getNoteNumber()] = true;
            int offPos = gate * samplesPerBeat;
            samplesToOff[mm.getNoteNumber()] = jmax(0, offPos);
            update = true;
        }
        
        event = mIterator.getNextEvent(mm, eventSamplePos);
    }
    
    return update;
}

int Arpeggiator::getNumNotesOn()
{
    int num = 0;
    for(int i = 0; i < 128; i++)
    {
        if(noteOn[i] || latchNotes[i])
            num++;
    }
    return num;
}

bool Arpeggiator::bufferSpansNextBeat(AudioSampleBuffer& buffer)
{
    int bufferSize = buffer.getNumSamples();
    int samplesUntilNextBeat = getSamplesToNextBeat();
    return bufferSize >= samplesUntilNextBeat ? true : false;
}

int Arpeggiator::getSamplesToNextBeat()
{
    return ((beatNum + 1) * samplesPerBeat) - sampleCount;
}

int Arpeggiator::getSamplesToOff()
{
    return (((beatNum + 1) * samplesPerBeat) + (gate * samplesPerBeat)) - sampleCount;
}

void Arpeggiator::timerCallback()
{
    clearNotes = true;
    stopTimer();
}

void Arpeggiator::setListener (Listener* newListener)
{
    listener = newListener;
}