//
//  MasterLevelSection.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 24/11/2015.
//
//

#ifndef MasterLevelSection_hpp
#define MasterLevelSection_hpp

/**
 Master level section UI
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"
#include "PresetSection.hpp"

class MasterLevelSection : public Component,
public Slider::Listener,
public PresetSection::Listener
{
public:
    /** Constructor.
     @param audio_ A reference to the Audio class @see Audio
     @param presetSection_ A reference to the PresetSection object so this class can listen to it @see PresetSection
     */
    MasterLevelSection(Audio& audio_, PresetSection& presetSection_);
    
    /** Destructor. */
    ~MasterLevelSection();
    
    /** Overridden from Component. */
    void paint(Graphics&) override;
    
    /** Overridden from Component. */
    void resized() override;
    
    /** Slider listener.
     @param slider The slider that has changed.
     */
    void sliderValueChanged (Slider* slider) override;
    
    /** PresetSection listener.
     @param currentPreset A ValueTree object containing the selected preset so this section can update its UI
     */
    void shouldUpdate(ValueTree currentPreset) override;
    
private:
    Audio& audio; /// A reference to the Audio class
    PresetSection& presetSection; /// A reference to the PresetSection class
    Label masterLevelLabel;
    Slider masterLevelSlider;
    
    Identifier masterLevelIdentifier; /// An array of this sections identifiers so it can update from the value tree
};

#endif /* MasterLevelSection_hpp */
