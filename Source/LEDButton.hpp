//
//  LEDButton.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 25/11/2015.
//
//

#ifndef LEDButton_hpp
#define LEDButton_hpp

/**
 Custom button using an LED image
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class LEDButton : public Button,
                  public Timer
{
public:
    /** Constructor. */
    LEDButton(const String& componentName = String::empty);
    
    /** Destructor. */
    ~LEDButton();
    
    /** Inherited from button. Paints the button. */
    void paintButton (Graphics& g,
                      bool isMouseOverButton,
                      bool isButtonDown) override;
    
    /** Flashs the button. */
    void flash();
    
    /** Inherited timer callback function. */
    void timerCallback() override;
private:
    Image upImage; /// The image for the button's "up" state
    Image downImage; /// The image for the button's "down" state
    
    float flashAlpha; /// The alpha value for button flash 
};

#endif /* LEDButton_hpp */
