//
//  AudioParameters.h
//  SDA_Assignment
//
//  Created by Edward Davies on 30/11/2015.
//
//

#ifndef AudioParameters_h
#define AudioParameters_h
#define NUMOSCS 3

/// An enum containing the parameter names
enum Parameters
{
    pMasterLevel = 0,
    pOscLevel0,
    pOscLevel1,
    pOscLevel2,
    pAAttack,
    pADecay,
    pASustain,
    pARelease,
    pFAttack,
    pFDecay,
    pFSustain,
    pFRelease,
    pFilterQ,
    pFilterDepth,
    pArpeggiatorGate,
    pSampleRate,
    
    pOctaveShift0,
    pOctaveShift1,
    pOctaveShift2,
    pDetuneAmount0,
    pDetuneAmount1,
    pDetuneAmount2,
    pOscWaveshape0,
    pOscWaveshape1,
    pOscWaveshape2,
    pFilterType,
    pArpeggiatorPattern,
    pFilterCutoff,
    pArpeggiatorBPM,
    pArpeggiatorResolution,
    pArpeggiatorSwing,
    pArpeggiatorVelocity,
    
    pOscEnabled0,
    pOscEnabled1,
    pOscEnabled2,
    pBeatActive0,
    pBeatActive1,
    pBeatActive2,
    pBeatActive3,
    pBeatActive4,
    pBeatActive5,
    pBeatActive6,
    pBeatActive7,
    pArpeggiatorState,
    pArpeggiatorLatchState,
    pArpeggiatorVelocityState,
    pArpeggiatorException,
    pArpeggiatorLatchException,
    pClearFilters,
    
    pReset
};

/// A struct containing the variables needed when using the FIFO buffer 
struct ParameterPair {
    Parameters identifier;
    
    union {
        float fVal;
        int iVal;
        bool bVal;
    };
    
    enum Type {
        Int,
        Float,
        Bool
    };
    Type t;
};

/// Floating-point parameter
/**
 * This class contains a set of functions for storing a floating-point audio parameter.
 */
class FloatParameter
{
public:
    /** Constructor
        @param defaultParameterValue The default value for this parameter
        @param paramName The name for this parameter from the Parameters enum
        @param coeff The smoothing coefficient (between 0 and 1), determines the weighting of the previous value and therefore the smoothing amount e.g. a value of 1 will not be smoothed
     */
    FloatParameter (float defaultParameterValue, Parameters paramName, float coeff)
    : defaultValue (defaultParameterValue), value (defaultParameterValue), name (paramName), a(coeff), b(1 - a), previous(defaultParameterValue)
    {
        
    }
    
    /** Returns the smoothed value
        @return The smoothed value
    */
    float getValue()
    {
        return previous = (a * value) + (b * previous);
    }
    
    /** Returns the non-smoothed value 
        @return The non-smoothed value
     */
    float getNonSmoothedValue()
    {
        return value;
    }
    
    /** Sets the value 
        @param newValue The new value for this parameter
     */
    void setValue (float newValue)
    {
        value = newValue;
    }
    
    /** Returns the default value 
        @return This parameter's default value
     */
    float getDefaultValue() const
    {
        return defaultValue;
    }
    
    /** Returns the parameter's name 
        @return The parameter's name from the Paramters enum
     */
    Parameters getName()
    {
        return name;
    }

private:
    float defaultValue; /// The default parameter value
    float value; /// Holds the current paramter value
    Parameters name; /// The paramter's name from the Parameters enum
    float a; /// The smoothing weighting for the current value;
    float b; /// The smoothing weighting for the previous value
    float previous; /// Holds the previous value when smoothing
};

const float defaultMasterLevel = 1.0f;
const float defaultOscLevel = 0.5 / NUMOSCS;

const float defaultAAttack = 0.1;
const float defaultADecay = 0.1;
const float defaultASustain = 1.f;
const float defaultARelease = 0.1;

const float defaultFAttack = 0.3;
const float defaultFDecay = 0.3;
const float defaultFSustain = 1.f;
const float defaultFRelease = 0.1;

const float defaultFilterQ = 0.7;
const float defaultFilterDepth = 0.f;

const float defaultSampleRate = 44100.f;

const float defaultArpeggiatorGate = 0.5;

//==========================================================================================

/// Integer parameter
/**
 * This class contains a set of functions for storing an integer audio parameter.
 */
class IntegerParameter
{
public:
    /** Constructor. 
        @param defaultParameterValue The default value for this parameter
        @param minValue The minimum value for the paramter
        @param maxValue The maximum value for the parameter
        @param paramName The name for this parameter from the Parameters enum
        @param coeff The smoothing coefficient (between 0 and 1), determines the weighting of the previous value and therefore the smoothing amount e.g. a value of 1 will not be smoothed
     */
    IntegerParameter (int defaultParameterValue, int minValue, int maxValue, Parameters paramName, float coeff) : defaultValue (defaultParameterValue), value (defaultParameterValue), min(minValue), max(maxValue), name (paramName), a(coeff), b(1 - a), previous(defaultParameterValue)
    {
        
    }
    
    /** Returns the smoothed value if it is between the min and max values.
        @return The smoothed value. If > maxValue returns maxValue, if < minValue returns minValue
     */
    
    int getValue()
    {
        if(value > max)
            return max;
        else if(value < min)
            return min;
        
        return previous = (a * value) + (b * previous);
    }
    
    /** Returns the non-smoothed value.
        @return The non-smoothed value. If > maxValue returns maxValue, if < minValue returns minValue
     */
    
    float getNonSmoothedValue()
    {
        if(value > max)
            return max;
        else if(value < min)
            return min;
        
        return value;
    }
    
    /** Sets the value
     @param newValue The new value for this parameter
     */
    void setValue (int newValue)
    {
        value = newValue;
    }
    
    /** Returns the default value
     @return This parameter's default value
     */
    int getDefaultValue() const
    {
        return defaultValue;
    }
    
    /** Returns the parameter's name
     @return The parameter's name from the Paramters enum
     */
    Parameters getName()
    {
        return name;
    }
    
private:
    int defaultValue; /// The default parameter value
    int value; /// Holds the current paramter value
    int min; /// The parameter's minimum value
    int max; /// The paramter's maximum value
    Parameters name; /// The paramter's name from the Parameters enum
    float a; /// The smoothing weighting for the current value;
    float b; /// The smoothing weighting for the previous value
    int previous; /// Holds the previous value when smoothing
};

const int defaultFilterCutoff = 20000;
const int defaultOctaveShift = 0;
const int defaultDetune = 0;
const int defaultFilterType = 0;
const int defaultArpeggiatorBPM = 120;
const int defaultArpeggiatorPattern = 1;
const int defaultArpeggiatorResolution = 8;
const int defaultOscWaveshape = 0;
const int defaultArpeggiatorSwing = 0;
const int defaultArpeggiatorVelocity = 127;

//==========================================================================================

/// Boolean parameter
/**
 * This class contains a set of functions for storing a boolean audio parameter.
 */
class BooleanParameter
{
public:
    /** Constructor
     @param defaultParameterValue The default value for this parameter
     @param paramName The name for this parameter from the Parameters enum
     */
    BooleanParameter (bool defaultParameterValue, Parameters paramName)
    : defaultValue (defaultParameterValue),
    value (defaultParameterValue),
    name (paramName)
    {
        
    }
    
    /** Returns the value 
        @param The paramter's current value
     */
    bool getValue() const
    {
        return value;
    }
    
    /** Sets the value
     @param newValue The new value for this parameter
     */
    void setValue (bool newValue)
    {
        value = newValue;
    }
    
    /** Returns the default value
     @return This parameter's default value
     */
    float getDefaultValue() const
    {
        return defaultValue;
    }
    
    /** Returns the parameter's name
     @return The parameter's name from the Paramters enum
     */
    Parameters getName()
    {
        return name;
    }
    
private:
    bool defaultValue;
    bool value;
    Parameters name;
};

const bool defaultOscEnabled = true;
const bool defaultBeatActive = true;
const bool defaultArpeggiatorState = false;
const bool defaultArpeggiatorLatchState = false;
const bool defaultArpeggiatorException = false;
const bool defaultArpeggiatorLatchException = false;
const bool defaultArpeggiatorVelocityState = true;
const bool defaultClearFilters = false;

//==========================================================================================

#endif /* AudioParameters_h */
