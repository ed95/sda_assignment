//
//  BiQuadFilter.hpp
//  SDA_Assignment
//
//  Created by Edward Davies on 28/11/2015.
//
//

#ifndef BiQuadFilter_hpp
#define BiQuadFilter_hpp

/**
 Base class for filters
 */

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.hpp"

class BiQuadFilter
{
public:
    /** Constructor.
        @param audio_ A reference to the Audio class @see Audio
     */
    BiQuadFilter(Audio& audio_);
    
    /** Destructor. */
    virtual ~BiQuadFilter();
    
    /** Returns the Q value.
        @return The filter's Q value
     */
    float getQ();
    
    /** Returns the sample rate.
        @return The current sample rate
     */
    float getSampleRate();
    
    /** Virtual function to set the filter cutoff.
        @param frequency The desired cutoff frequency
     */
    virtual void setCutoff(float frequency) = 0;
    
    /** Sets the filter coefficients.
        @param b0 The b0 coefficient
        @param b1 The b1 coefficient
        @param b2 The b2 coefficient
        @param a1 The a1 coefficient
        @param a2 The a2 coefficient
     */
    void setCoeffs(float b0, float b1, float b2, float a1, float a2);
    
    /** Outputs a filtered sample based on an input sample 
        @param inputSample The input sample
        @return The filtered output sample
     */
    float outputFiltered(float inputSample);
    
    void clearFilter();
    
private:
    stk::BiQuad filter; /// base filter
    Audio& audio; /// reference to Audio object
    float sampleRate; /// sample rate
    float qVal; /// filter Q value
};

#endif /* BiQuadFilter_hpp */
